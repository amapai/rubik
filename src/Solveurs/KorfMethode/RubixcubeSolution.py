 #-*- coding: utf-8 -*-
from ScrambleRubixcube import xInitial, make_move
import numpy as np

import time


array = np.array([
    [[0, 0, 2], [1, 0, 2], [2, 0, 2]],
    [[0, 0, 1], [1, 0, 1], [2, 0, 1]],
    [[0, 0, 0], [1, 0, 0], [2, 0, 0]],
    [[0, 0, 2], [0, 1, 2], [0, 2, 2]],
    [[0, 0, 1], [0, 1, 1], [0, 2, 1]],
    [[0, 0, 0], [0, 1, 0], [0, 2, 0]],
    [[0, 0, 0], [1, 0, 0], [2, 0, 0]],
    [[0, 1, 0], [1, 1, 0], [2, 1, 0]],
    [[0, 2, 0], [1, 2, 0], [2, 2, 0]],
    [[2, 0, 0], [2, 0, 1], [2, 0, 2]],
    [[2, 1, 0], [2, 1, 1], [2, 1, 2]],
    [[2, 2, 0], [2, 2, 1], [2, 2, 2]],
    [[2, 0, 2], [1, 0, 2], [0, 0, 2]],
    [[2, 1, 2], [1, 1, 2], [0, 1, 2]],
    [[2, 2, 2], [1, 2, 2], [0, 2, 2]],
    [[0, 2, 0], [1, 2, 0], [2, 2, 0]],
    [[0, 2, 1], [1, 2, 1], [2, 2, 1]],
    [[0, 2, 2], [1, 2, 2], [2, 2, 2]],
])


class State:
    cube = None
    g = 0
    h = 0
    parent = None
    move = None


# checks if goal reached. if reached writes goal state in output.txt
def goal_reached(curr):
    if curr.h != 0:
        return False

    return True
    # goal reached
    cube = curr.cube
    file = open('output.txt', 'w')
    file.write("              " + str(cube[0, 0:3]) + '\n')
    file.write("              " + str(cube[1, 0:3]) + '\n')
    file.write("              " + str(cube[2, 0:3]) + '\n')
    file.write(str(cube[3, 0:3]) + ' ' + str(cube[6, 0:3]) + ' ' + str(cube[9, 0:3]) + ' ' + str(cube[12, 0:3]) + '\n')
    file.write(str(cube[4, 0:3]) + ' ' + str(cube[7, 0:3]) + ' ' + str(cube[10, 0:3]) + ' ' + str(cube[13, 0:3]) + '\n')
    file.write(str(cube[5, 0:3]) + ' ' + str(cube[8, 0:3]) + ' ' + str(cube[11, 0:3]) + ' ' + str(cube[14, 0:3]) + '\n')
    file.write("              " + str(cube[15, 0:3]) + '\n')
    file.write("              " + str(cube[16, 0:3]) + '\n')
    file.write("              " + str(cube[17, 0:3]) + '\n')



# checks if child ascendant of parent
def contains1(child, parent):
    curr = parent.parent
    while curr is not None:
        if np.array_equal(curr.cube, child): return True
        curr = curr.parent

    return False


# checks if frontier contains child
def contains2(child, frontier):
    for curr in frontier:
        if np.array_equal(curr.cube, child): return True

    return False


def ida(start):
    start.h = corner_edge_sum_max(start.cube)
    cost_limit = start.h
    nodes = 0
    frontier = list()
    branching_factors = list()

    deb = time.time()
    fin = 300.0

    while True:
        minimum = None
        frontier.append(start)
        #print(nodes)

        actuel = time.time()
        while len(frontier) != 0:
            curr = frontier.pop()

            actuel = time.time()

            if goal_reached(curr):

                tmp = ""
                while curr is not None:
                    if curr.move is not None:
                        tmp += curr.move + " "
                    curr = curr.parent
                return tmp
            if actuel - deb > fin:
                print("Trop long, plus de " + str(fin) + "s")
                return ""

            b = 0
            nodes = nodes + 12
            for i in range(12):
                new = State()
                new.cube = np.array(curr.cube)
                new.g = curr.g + 1
                new.parent = curr
                new.move = make_move(new.cube, i + 1, 0)
                new.h = corner_edge_sum_max(new.cube)

                if new.g + new.h > cost_limit:
                    if minimum is None or new.g + new.h < minimum:
                        minimum = new.g + new.h
                    continue
                if curr.parent is not None and (contains1(new.cube, curr) or contains2(new.cube, frontier)):
                    continue
                frontier.append(new)
                b = b + 1
            if b != 0:
                branching_factors.append(b)

        cost_limit = minimum


def manhattan_distance(cube, i, z, corner):

    c1 = array[i, z]
    center = None
    for c in [1, 4, 7, 10, 13, 16]:
        if cube[i, z] == cube[c, 1]:
            center = c
            break

    if corner:
        c2 = array[center - 1, 0]
        d1 = abs(c1[0] - c2[0]) + abs(c1[1] - c2[1]) + abs(c1[2] - c2[2])
        c2 = array[center - 1, 2]
        d2 = abs(c1[0] - c2[0]) + abs(c1[1] - c2[1]) + abs(c1[2] - c2[2])
        c2 = array[center + 1, 0]
        d3 = abs(c1[0] - c2[0]) + abs(c1[1] - c2[1]) + abs(c1[2] - c2[2])
        c2 = array[center + 1, 2]
        d4 = abs(c1[0] - c2[0]) + abs(c1[1] - c2[1]) + abs(c1[2] - c2[2])
        return min(d1, d2, d3, d4)
    else:
        c2 = array[center - 1, 1]
        d1 = abs(c1[0] - c2[0]) + abs(c1[1] - c2[1]) + abs(c1[2] - c2[2])
        c2 = array[center, 0]
        d2 = abs(c1[0] - c2[0]) + abs(c1[1] - c2[1]) + abs(c1[2] - c2[2])
        c2 = array[center, 2]
        d3 = abs(c1[0] - c2[0]) + abs(c1[1] - c2[1]) + abs(c1[2] - c2[2])
        c2 = array[center + 1, 1]
        d4 = abs(c1[0] - c2[0]) + abs(c1[1] - c2[1]) + abs(c1[2] - c2[2])
        return min(d1, d2, d3, d4)


def corner_edge_sum_max(cube):
    corners = 0
    edges = 0
    for i in range(18):
        if i % 3 == 0 or i % 3 == 2:
            corners = corners + manhattan_distance(cube, i, 0, True) + manhattan_distance(cube, i, 2, True)
            edges = edges + manhattan_distance(cube, i, 1, False)
        else:
            edges = edges + manhattan_distance(cube, i, 0, False) + manhattan_distance(cube, i, 2, False)
    return max(corners / 12, edges / 8)


##########################################
def initCube(cube, chaineCube):
    indexes = [0, 1, 2, 3, 6, 9, 12, 4, 7, 10, 13, 5, 8, 11, 14, 15, 16, 17]
    index = 0
    tmp = ""

    for cpt in range(0,len(chaineCube),3):
        tmp += chaineCube[cpt] + chaineCube[cpt+1] + chaineCube[cpt+2]
        i = indexes[index]
        cube.cube[i, 0] = chaineCube[cpt]
        cube.cube[i, 1] = chaineCube[cpt+1]
        cube.cube[i, 2] = chaineCube[cpt+2]
        index = index + 1

    # pour la longueur de la chaine
    #

def initCubeInputRGBinitCube(cube):

    handle = open('inputRGB.txt')
    indexes = [0, 1, 2, 3, 6, 9, 12, 4, 7, 10, 13, 5, 8, 11, 14, 15, 16, 17]
    index = 0
    tmp = ""

    for line in handle:
        line = line.replace(' ', '')
        for row in line.split('['):
            if len(row) != 0:
                tmp += row[1] + row[4] + row[7]
                i = indexes[index]
                cube.cube[i, 0] = row[1]
                cube.cube[i, 1] = row[4]
                cube.cube[i, 2] = row[7]
                index = index + 1

    print(tmp)

def decompostionClearToRGB(chaine):
    # WWWWWWWWW BBBBBBBBB RRRRRRRRR YYYYYYYYY GGGGGGGGG OOOOOOOOO
    # =>      8         17        26        35        44        53
    # WWWWWWWWW GGGRRRBBB OOOGGGRRR BBBOOOGGG RRRBBBOOO YYYYYYYYY
    # 123456789 123123123 123456456 456456789 789789789 123456789

    retour = ""
    retour += chaine[0:9]
    retour += chaine[36:39]
    retour += chaine[18:21]
    retour += chaine[9:12]
    retour += chaine[45:48]
    retour += chaine[39:42]
    retour += chaine[21:24]
    retour += chaine[12:15]
    retour += chaine[48:51]
    retour += chaine[42:45]
    retour += chaine[24:27]
    retour += chaine[15:18]
    retour += chaine[51:54]
    retour += chaine[27:36]

    return retour;

def correctToRGB(chaine):
    chaine = chaine.replace("U", "WW")
    chaine = chaine.replace("L", "GG")
    chaine = chaine.replace("B", "OO")
    chaine = chaine.replace("R", "BB")
    chaine = chaine.replace("F", "RR")
    chaine = chaine.replace("D", "YY")

    chaine = chaine.replace("WW", "W")
    chaine = chaine.replace("GG", "G")
    chaine = chaine.replace("OO", "O")
    chaine = chaine.replace("BB", "B")
    chaine = chaine.replace("RR", "R")
    chaine = chaine.replace("YY", "Y")

    return chaine

def RBGToCorrect(chaine):
    chaine = chaine.replace("W", "UU")
    chaine = chaine.replace("G", "LL")
    chaine = chaine.replace("R", "FF")
    chaine = chaine.replace("B", "RR")
    chaine = chaine.replace("O", "BB")
    chaine = chaine.replace("Y", "DD")

    chaine = chaine.replace("UU","U")
    chaine = chaine.replace("LL","L")
    chaine = chaine.replace("FF","F")
    chaine = chaine.replace("RR","R")
    chaine = chaine.replace("BB","B")
    chaine = chaine.replace("DD","D")

    return chaine

def idaLaunch(chaineCube):
    chaineCube = decompostionClearToRGB(correctToRGB(chaineCube))
    # print(chaineCube)
    # Initialize état algo
    curr = State()

    curr.cube = np.array(xInitial)

    initCube(curr, chaineCube)

    # goal reached
    return ida(curr)


    #https://github.com/FarhanShoukat/Rubiks-Cube-Solver
