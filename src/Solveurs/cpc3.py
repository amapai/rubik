 #-*- coding: utf-8 -*-
import random
from generateur import *


class CubeSolveur(Cube):
    def __init__(self, colours):
        self.suiteCoupsCpC = ""
        self.nbCoups = 0
        Cube.__init__(self,colours)


    #Renvoie true si le cube est resolu, false sinon
    def resolu(self):
        return (self.D.resolu() \
         and self.F.resolu() \
         and self.R.resolu() \
         and self.L.resolu() \
         and self.B.resolu() \
         and self.U.resolu())


    #Algorithme de la chaise
    #Seffectue sur la face passee en parametre et du cote passe en prametre
    def chaise(self, face, cote):
        self.nbCoups += 7
        if face == "F":
            if cote == "D":
                self.rp(); self.d(); self.d(); self.r(); self.d(); self.rp(); self.d(); self.r()
                self.suiteCoupsCpC += "R' D2 R D R' D R "
            else :
                self.l(); self.d(); self.d(); self.lp(); self.dp(); self.l(); self.dp(); self.lp()
                self.suiteCoupsCpC += "L D2 L' D' L D' L' "
        elif face == "R":
            if cote == "D":
                self.bp(); self.d(); self.d(); self.b(); self.d(); self.bp(); self.d(); self.b()
                self.suiteCoupsCpC += "B' D2 B D B' D B "
            else :
                self.f(); self.d(); self.d(); self.fp(); self.dp(); self.f(); self.dp(); self.fp()
                self.suiteCoupsCpC += "F D2 F' D' F D' F' "
        elif face == "L":
            if cote == "D":
                self.fp(); self.d(); self.d(); self.f(); self.d(); self.fp(); self.d(); self.f()
                self.suiteCoupsCpC += "F' D2 F D F' D F "
            else:
                self.b(); self.d(); self.d(); self.bp(); self.dp(); self.b(); self.dp(); self.bp()
                self.suiteCoupsCpC += "B D2 B' D' B D' B' "
        elif face == "B":
            if cote == "D":
                self.lp(); self.d(); self.d(); self.l(); self.d(); self.lp(); self.d(); self.l()
                self.suiteCoupsCpC += "L' D2 L2 D L' D L "
            else:
                self.r(); self.d(); self.d(); self.rp(); self.dp(); self.r(); self.dp(); self.rp()
                self.suiteCoupsCpC += "R D2 R' D' R D' R' "
        else :
            print("Face non reconnue - Chaise")

    #Algorithme de la double chaise
    def doubleChaise(self, face):
        if face == "F":
            self.chaise("F", "G")
            self.chaise("F", "D")
        elif face == "R":
            self.chaise("R", "G")
            self.chaise("R", "D")
        elif face == "L":
            self.chaise("L", "G")
            self.chaise("L", "D")
        elif face == "B":
            self.chaise("B", "G")
            self.chaise("B", "D")
        else:
            print("Face non reconnue - Double chaise")

    #Determine sur quelle face appliquer la double chaise
    def checkStateOrientationCoin(self):
        #On cherche les deux bandes jaunes
        if self.F.resolu() and self.R.c7.colour == "R" and self.L.c9.colour == "L":
            #print("Cas 1")
            return "L"
        elif self.L.resolu() and self.F.c7.colour == "F" and self.B.c9.colour == "B":
            #print("Cas 2")
            return "B"
        elif self.B.resolu() and self.L.c7.colour == "L" and self.R.c9.colour == "R":
            #print("Cas 3")
            return "R"
        elif self.R.resolu() and self.B.c7.colour == "B" and self.F.c9.colour == "F":
            #print("Cas 4")
            return "F"

        #Sinon on cherche un coin bien oriente
        elif self.F.c7.colour == "F" and  self.L.c9.colour == "L" and self.D.c1.colour == "D":
            #print("Cas 5")
            return "L"
        elif self.L.c7.colour == "L" and self.B.c9.colour == "B" and self.D.c7.colour == "D":
            #print("Cas 6")
            return "B"
        elif self.B.c7.colour == "B" and self.R.c9.colour == "R" and self.D.c9.colour == "D":
            #print("Cas 7")
            return "R"
        elif self.R.c7.colour == "R" and self.F.c9.colour == "F" and self.D.c3.colour == "D":
            #print("Cas 8")
            return "F"
        #Sinon double chaise depuis F
        else :
            #print("Cas default")
            return "F"

    #etape dorientation des angles
    def step7(self):
        #print("\nDébut de l'étape 7 : Orientation des coins")
        while (not(self.resolu())):
            face = self.checkStateOrientationCoin()
            self.doubleChaise(face)
            #print("Step7 - une etape de plus - "+ face)
        #print("Le cube est resolu !")
        #print("Nombres de coups : "+str(self.nbCoups))


    #Algoithme des voisins pour une face - coin en bas a droite bien positionne
    def voisins(self, face):
        self.nbCoups += 8
        if face == "F":
            self.l(); self.dp(); self.rp(); self.d(); self.lp(); self.dp(); self.r(); self.d()
            self.suiteCoupsCpC += "L D' R' D L' D' R D "
        elif face == "R":
            self.r(); self.dp(); self.lp(); self.d(); self.rp(); self.dp(); self.l(); self.d()
            self.suiteCoupsCpC += "R D' L' D R' D' L D "
        elif face == "L":
            self.b(); self.dp(); self.fp(); self.d(); self.bp(); self.dp(); self.f(); self.d()
            self.suiteCoupsCpC += "B D' F' D B' D' F D "
        elif face == "B":
            self.f(); self.dp(); self.bp(); self.d(); self.fp(); self.dp(); self.b(); self.d()
            self.suiteCoupsCpC += "F D' B' D F' D' B D "
        else :
            print("Face non reconnue - voisins")

    #TODO corriger in
    def checkPlacementCoin(self):
        if (self.F.c7.colour in ["F","L","D"] and self.L.c9.colour in ["F", "L", "D"] and self.D.c1.colour in ["F", "L", "D"]):
            #print("cas 1")
            return "L"

        elif (self.F.c9.colour in ["F", "R", "D"] and self.R.c7.colour in ["F", "R", "D"] and self.D.c3.colour in ["F", "R", "D"]):
            #print("cas 2")
            return "F"

        elif (self.R.c9.colour in ["R", "B", "D"] and self.B.c7.colour in ["R", "B", "D"] and self.D.c9.colour in ["R", "B", "D"]):
            #print("cas 3")
            return "R"
        elif (self.L.c7.colour in ["L", "B", "D"] and self.B.c9.colour in ["L", "B", "D"] and self.D.c7.colour in ["L", "B", "D"]):
            #print("cas 4")
            return "B"

        else:
            return "F"

    #Verifie si les angles jaunes sont bien places
    # TODO corriger in self.B.colourFace
    def coinBienPlaces(self):
        return (self.F.c7.colour in [self.F.colourFace, self.L.colourFace, self.D.colourFace]\
             and self.L.c9.colour in [self.F.colourFace, self.L.colourFace, self.D.colourFace]\
             and self.D.c1.colour in [self.F.colourFace, self.L.colourFace, self.D.colourFace])\
    		 and(self.R.c9.colour in [self.B.colourFace, self.R.colourFace, self.D.colourFace]\
             and self.B.c7.colour in [self.B.colourFace, self.R.colourFace, self.D.colourFace]\
             and self.D.c9.colour in [self.B.colourFace, self.R.colourFace, self.D.colourFace])\
    		 and(self.B.c9.colour in [self.B.colourFace, self.L.colourFace, self.D.colourFace]\
             and self.L.c7.colour in [self.B.colourFace, self.L.colourFace, self.D.colourFace]\
             and self.D.c7.colour in [self.B.colourFace, self.L.colourFace, self.D.colourFace])

        #(self.F.c7.colour in ["F", "L", "D"] and self.L.c9.colour in ["F", "L", "D"] and self.D.c1.colour in ["F", "L", "D"]) \
		#and (self.F.c9.colour in ["F", "R", "D"] and self.R.c7.colour in ["F", "R", "D"] and self.D.c3.colour in ["F", "R", "D"]) \
		#and (self.R.c9.colour in ["R", "B", "D"] and self.B.c7.colour in ["R", "B", "D"] and self.D.c9.colour in ["R", "B", "D"]) \
		#and (self.L.c7.colour in ["L", "B", "D"] and self.B.c9.colour in ["L", "B", "D"] and self.D.c7.colour in ["L", "B", "D"])

    #Etape de positionnement des angles
    def step6(self):
        #print("\nDébut de l'étape 6 - positionnement des angles")
        while (not(self.coinBienPlaces())):
            face = self.checkPlacementCoin()
            self.voisins(face)
            #print("Step6 - une etape de plus - "+ face)
        #print("Angles bien positionnes !")
        #print("Nombre de coups : "+str(self.nbCoups))

    #verifie si les centres jaunes sont alignes
    def centreAligne(self):
        i = 0
        while ( i < 5 and not(self.F.c8.colour == self.F.colourFace \
            and self.R.c8.colour == self.R.colourFace \
            and self.L.c8.colour == self.L.colourFace \
            and self.B.c8.colour == self.B.colourFace)):
                self.d()
                self.suiteCoupsCpC += "D "
                self.nbCoups+=1
                i = i+1

        return not(i==5)

    #Verifie si certains centres sont deja alignes
    #Revoie la face depuis laquelle faire la chaise pour aligner les autres
    def checkCentre(self):
        i = 0
        while (i < 4):
            if self.F.c8.colour == self.F.colourFace and self.R.c8.colour == self.R.colourFace:
                #print("Cas 1")
                return "L"

            elif self.F.c8.colour == self.F.colourFace and self.L.c8.colour == self.L.colourFace:
                #print("Cas 2")
                return "B"

            elif self.L.c8.colour == self.L.colourFace and self.B.c8.colour == self.B.colourFace:
                #print("Cas 3")
                return "R"

            elif self.B.c8.colour == self.B.colourFace and self.R.c8.colour == self.R.colourFace:
                #print("Cas 4")
                return "F"

            elif self.F.c8.colour == self.F.colourFace and self.B.c8.colour == self.B.colourFace:
                #print("Cas 5")
                return "F"

            elif self.R.c8.colour == self.R.colourFace and self.L.c8.colour == self.L.colourFace:
                #print("Cas 6")
                return "R"

            else:
                #print("cas default - i = "+str(i))
                i = i+1
                self.d()
                self.suiteCoupsCpC += "D "
                self.nbCoups+=1

    #effectue lalignement des centres jaunes
    def step5(self):
        #print("\nDébut de l'étape 5 - Alignement des centres de la face D")
        while (not(self.centreAligne())):
            face = self.checkCentre()
            self.chaise(face, "G")
            #print("Step5 - une etape de plus - "+ face)
        #print("Les centres sont alignes !")
        #print("Nombre de coups : "+str(self.nbCoups))

    #vereifie si la croix jaune est faite
    def croixJaune(self):
        return (self.D.c2.colour == self.D.colourFace \
                and self.D.c4.colour == self.D.colourFace \
                and self.D.c6.colour == self.D.colourFace \
                and self.D.c8.colour == self.D.colourFace )

    #Positionne la face jaune pour appliquer les chinois
    def checkCroix(self):
        if (self.D.c4.colour == self.D.colourFace and self.D.c6.colour == self.D.colourFace):
            #print("cas 1")
            self.d()
            self.suiteCoupsCpC += "D "
            self.nbCoups+=1
        elif (self.D.c6.colour == self.D.colourFace and self.D.c8.colour == self.D.colourFace):
            #print("cas 2")
            self.dp()
            self.suiteCoupsCpC += "D' "
            self.nbCoups+=1
        elif (self.D.c2.colour == self.D.colourFace and self.D.c4.colour == self.D.colourFace):
            #print("cas 3")
            self.d()
            self.suiteCoupsCpC += "D "
            self.nbCoups+=1
        elif (self.D.c4.colour == self.D.colourFace and self.D.c8.colour == self.D.colourFace):
            #print("cas 4")
            self.d(); self.d()
            self.suiteCoupsCpC += "D2 "
            self.nbCoups+=2
        else:
            #print("cas default - checkCroix")
            pass


    def chinois(self):
        self.l(); self.b(); self.d(); self.bp(); self.dp(); self.lp()
        self.suiteCoupsCpC += "L B D B' D' L "
        self.nbCoups+=6

    #cree la croix jaune
    def step4(self):
        #print("\nDébut de l'étape 4 : creation de la croix de la face D")
        while (not(self.croixJaune())):
            self.checkCroix()
            self.chinois()
            #print("Step4 - une etape de plus")
        #print("La croix est faite !")
        #print("Nombre de coups : "+str(self.nbCoups))

    #Methode du belge
    #D signifie que le cube part dans le sens horaire
    def belge(self, face, cote):
        self.nbCoups += 8
        if face == "F":
            if cote == "D":
                self.d(); self.l(); self.dp(); self.lp(); self.dp(); self.fp(); self.d(); self.f()
                self.suiteCoupsCpC += "D L D' L' D' F' D F "
            else :
                self.dp(); self.rp(); self.d(); self.r(); self.d(); self.f(); self.dp(); self.fp()
                self.suiteCoupsCpC += "D' R' D R D F D' F' "
        elif face == "R":
            if cote == "D":
                self.d(); self.f(); self.dp(); self.fp(); self.dp(); self.rp(); self.d(); self.r()
                self.suiteCoupsCpC += "D F D' F' D' R' D R "
            else :
                self.dp(); self.bp(); self.d(); self.b(); self.d(); self.r(); self.dp(); self.rp()
                self.suiteCoupsCpC += "D' B' D B D R D' R' "
        elif face == "L":
            if cote == "D":
                self.d(); self.b(); self.dp(); self.bp(); self.dp(); self.lp(); self.d(); self.l()
                self.suiteCoupsCpC += "D B D' B' D' L' D L "
            else:
                self.dp(); self.fp(); self.d(); self.f(); self.d(); self.l(); self.dp(); self.lp()
                self.suiteCoupsCpC += "D' F' D F D L D' L' "
        elif face == "B":
            if cote == "D":
                self.d(); self.r(); self.dp(); self.rp(); self.dp(); self.bp(); self.d(); self.b()
                self.suiteCoupsCpC += "D R D' R' D' B' D B "
            else:
                self.dp(); self.lp(); self.d(); self.l(); self.d(); self.b(); self.dp(); self.bp()
                self.suiteCoupsCpC += "D' L' D L D B D' B' "
        else :
            #print("Face non reconnue - Belge")
            pass
    #Revoie vrai si la deuximee couronne est faite
    def deuxiemeCouronne(self):
        return (self.F.c4.colour == self.F.colourFace and self.F.c6.colour == self.F.colourFace \
            and self.R.c4.colour == self.R.colourFace and self.R.c6.colour == self.R.colourFace \
            and self.L.c4.colour == self.L.colourFace and self.L.c6.colour == self.L.colourFace \
            and self.B.c4.colour == self.B.colourFace and self.B.c6.colour == self.B.colourFace )

    #positionne larete a placer et renvoit la face sur laquelle effectuer le belge
    def positionneArete(self):
        #Si aucun des deux nest jaune, on va le positionner a sa place
        #On regarde arete en haut de face F
        if self.D.c2.colour != self.D.colourFace and self.F.c8.colour != self.D.colourFace:
            if self.F.c8.colour == self.F.colourFace:
                return "F"
            elif self.F.c8.colour == self.R.colourFace:
                self.d()
                self.suiteCoupsCpC += "D "
                self.nbCoups+=1
                return "R"
            elif self.F.c8.colour == self.L.colourFace:
                self.dp()
                self.suiteCoupsCpC += "D' "
                self.nbCoups+=1
                return "L"
            elif self.F.c8.colour == self.B.colourFace:
                self.d(); self.d()
                self.suiteCoupsCpC += "D' "
                self.nbCoups+=2
                return "B"
            else:
                print("Couleur non reconnue")

        #On regarde arete en haut de face R
        elif self.D.c6.colour != self.D.colourFace and self.R.c8.colour != self.D.colourFace:
            if self.R.c8.colour == self.F.colourFace:
                self.dp()
                self.suiteCoupsCpC += "D' "
                self.nbCoups+=1
                return "F"
            elif self.R.c8.colour == self.R.colourFace:
                return "R"
            elif self.R.c8.colour == self.L.colourFace:
                self.d(); self.d()
                self.suiteCoupsCpC += "D2 "
                self.nbCoups+=2
                return "L"
            elif self.R.c8.colour == self.B.colourFace:
                self.d()
                self.suiteCoupsCpC += "D "
                self.nbCoups+=1
                return "B"
            else:
                print("Couleur non reconnue")

        #On regarde arete en haut de face L
        elif self.D.c4.colour != self.D.colourFace and self.L.c8.colour != self.D.colourFace:
            if self.L.c8.colour == self.F.colourFace:
                self.d()
                self.suiteCoupsCpC += "D "
                self.nbCoups+=1
                return "F"
            elif self.L.c8.colour == self.R.colourFace:
                self.d(); self.d()
                self.suiteCoupsCpC += "D2 "
                self.nbCoups+=2
                return "R"
            elif self.L.c8.colour == self.L.colourFace:
                return "L"
            elif self.L.c8.colour == self.B.colourFace:
                self.dp()
                self.suiteCoupsCpC += "D' "
                self.nbCoups+=1
                return "B"
            else:
                print("Couleur non reconnue")

        #On regarde la face B
        elif self.D.c8.colour != self.D.colourFace and self.B.c8.colour != self.D.colourFace:
            if self.B.c8.colour == self.F.colourFace:
                self.d(); self.d()
                self.suiteCoupsCpC += "D2 "
                self.nbCoups+=2
                return "F"
            elif self.B.c8.colour == self.R.colourFace:
                self.dp()
                self.suiteCoupsCpC += "D' "
                self.nbCoups+=1
                return "R"
            elif self.B.c8.colour == self.L.colourFace:
                self.d()
                self.suiteCoupsCpC += "D "
                self.nbCoups+=1
                return "L"
            elif self.B.c8.colour == self.B.colourFace:
                return "B"
            else:
                print("Couleur non reconnue")

        #Si jamais on a du jaune sur toutes les arete de la face du dessous
        #Alors on deloge une arete deja sur la couronne mais mal placee

        #On regarde larete FL
        elif (self.F.c4.colour != self.F.colourFace or self.L.c6.colour != self.L.colourFace):
            self.belge("F", "D")
            return self.positionneArete()

        #On regarde larete FR
        elif (self.F.c6.colour != self.F.colourFace or self.R.c4.colour != self.R.colourFace):
            self.belge("F", "G")
            return self.positionneArete()

        #On regarde larete RB
        elif (self.R.c6.colour != self.R.colourFace or self.B.c4.colour != self.B.colourFace):
            self.belge("R", "G")
            return self.positionneArete()

        #On regarde larete LB
        elif (self.L.c4.colour != self.L.colourFace or self.B.c6.colour != self.B.colourFace):
            self.belge("L", "D")
            return self.positionneArete()
        else:

            #print("Aucun cas detecte - positionneArete")
            pass
    #renvoie le sens dans lequel partir pour le belge
    def choixCote(self, face):
        #Regarde lautre cote de larete en position c8 de la face donne et determine si gauche ou droite
        if face == "F":
            if self.D.c2.colour == self.R.colourFace:
                return "G"
            else:
                return "D"
        elif face == "R":
            if self.D.c6.colour == self.B.colourFace:
                return "G"
            else:
                return "D"
        elif face == "L":
            if self.D.c4.colour == self.F.colourFace:
                return "G"
            else:
                return "D"
        elif face == "B":
            if self.D.c8.colour == self.L.colourFace:
                return "G"
            else:
                return "D"
        else:
            #print("Face non reconnue - choixCote")
            pass

    def step3(self):
        #print("\nDébut de l'étape 3 : la deuxieme couronne")
        while (not(self.deuxiemeCouronne())):
        #for i in range(0,3):
            #print("----------------------------")
            #self.toStringSpace()
            face = self.positionneArete()
            #print(face)
            cote = self.choixCote(face)
            #print(cote)
            self.belge(face, cote)
            #print("Step3 - une etape de plus - "+face+" - "+cote)
        #print("La deuxieme couronne est faite !")
        #print("nombre de coups : " + str(self.nbCoups))


    def step2(self):
        #print("\nDébut de l'étape 2 : Placement des coins blancs pour la première couronne")
        while (not self.cotesBlancOK()):
            self.cotesBlanche()

    def cotesBlancOK(self):
        return (self.U.c1.colour == self.U.colourFace and self.U.c3.colour == self.U.colourFace\
            and self.U.c5.colour == self.U.colourFace and self.U.c9.colour == self.U.colourFace\
            and self.F.c1.colour == self.F.colourFace and self.F.c3.colour == self.F.colourFace\
            and self.B.c1.colour == self.B.colourFace and self.B.c3.colour == self.B.colourFace\
            and self.L.c1.colour == self.L.colourFace and self.L.c3.colour == self.L.colourFace\
            and self.R.c1.colour == self.R.colourFace and self.R.c3.colour == self.R.colourFace)

    def cotesBlanche(self):
        # Coté 1 OK
        while (not (self.U.c1.colour == self.U.colourFace\
            and self.B.c3.colour == self.B.colourFace\
            and self.L.c1.colour == self.L.colourFace)):

            #print("coté 1 NOK")
            # Si bon mais inversé
            if (self.U.c1.colour in [self.B.colourFace, self.L.colourFace]\
            and self.B.c3.colour in [self.U.colourFace, self.L.colourFace]\
            and self.L.c1.colour in [self.B.colourFace, self.U.colourFace]):
                # Inversé
                if(self.B.c3.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("B D B' ")
                    self.b();self.d();self.bp()
                elif(self.L.c1.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("L' D L ")
                    self.lp();self.d();self.l()

            # Si sur autre coins
            if (self.U.c3.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.R.c3.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.B.c1.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]):
                #print"coté U.c3  ")
                self.suiteCoupsCpC +=("R D R' ")
                self.r();self.d();self.rp()

            if (self.U.c9.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.F.c3.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.R.c1.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]):
                #print"coté U.c9 ")
                self.suiteCoupsCpC +=("F D F' ")
                self.f();self.d();self.fp()

            if (self.U.c7.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.L.c3.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.F.c1.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]):
                #print"coté U.c7 ")
                self.suiteCoupsCpC +=("L D L' ")
                self.l();self.d();self.lp()

            # Si D sur oposé
            if (self.D.c3.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.R.c7.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.F.c9.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]):
                #print"coté D.c3 ")
                if(self.R.c7.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("D' ")
                    self.dp()
                elif(self.F.c9.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("D ")
                    self.d()
                elif(self.D.c3.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("D2 ")
                    self.d();self.d()

            # Si sur D sous bon coté
            if (self.D.c7.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.B.c9.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.L.c7.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]):
                #print"coté D.c7 ")
                if(self.B.c9.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("D' ")
                    self.dp()
                elif(self.L.c7.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("D ")
                    self.d()
                elif(self.D.c7.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("B D' B' ")
                    self.b();self.dp();self.bp()

            # Si bord droit
            if (self.D.c1.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.L.c9.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.F.c7.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]):
                #print"coté D.c1 ")
                # Si blanc dessous
                if(self.D.c1.colour == self.U.colourFace):
                    #print"Dessous ")
                    self.suiteCoupsCpC +=("D' ")
                    self.dp()

                # Si blanc mauvais coté
                if(self.L.c9.colour == self.U.colourFace):
                    #print"Bord droit orienté gauche ")
                    self.suiteCoupsCpC +=("D2 ")
                    self.d();self.d()

                # Si blanc OK
                if(self.F.c7.colour == self.U.colourFace):
                    #print"Bord droit OK ")
                    self.suiteCoupsCpC +=("B D' B' ")
                    self.b();self.dp();self.bp()

            # Si bord gauche
            if (self.D.c9.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.B.c7.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]\
            and self.R.c9.colour in [self.B.colourFace, self.L.colourFace, self.U.colourFace]):
                #print"coté D.c9 ")
                # Si blanc dessous
                if(self.D.c9.colour == self.U.colourFace):
                    #print"Dessous ")
                    self.suiteCoupsCpC +=("D ")
                    self.d()

                # Si blanc mauvais coté
                elif(self.B.c7.colour == self.U.colourFace):
                    #print"Bord gauche orienté droit ")
                    self.suiteCoupsCpC +=("D2 ")
                    self.d();self.d()

                # Si blanc OK
                elif(self.R.c9.colour == self.U.colourFace):
                    #print"Bord gauche OK ")
                    self.suiteCoupsCpC +=("L' D L ")
                    self.lp();self.d();self.l()

        # Second coin
        while (not (self.U.c3.colour == self.U.colourFace\
            and self.R.c3.colour == self.R.colourFace\
            and self.B.c1.colour == self.B.colourFace)):
            #print"\n\ncoté 2 NOK ")

            # Si bon mais inversé
            if (self.U.c3.colour in [self.R.colourFace, self.B.colourFace]\
            and self.R.c3.colour in [self.U.colourFace, self.B.colourFace]\
            and self.B.c1.colour in [self.R.colourFace, self.U.colourFace]):
                # Inversé
                if(self.R.c3.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("R D R' ")
                    self.r();self.d();self.rp()
                elif(self.B.c1.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("B' D B ")
                    self.bp();self.d();self.b()

            # Si sur autre coins
            if (self.U.c9.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.F.c3.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.R.c1.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]):
                #print"coté U.c9 ")
                self.suiteCoupsCpC +=("F D F' ")
                self.f();self.d();self.fp()

            if (self.U.c7.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.L.c3.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.F.c1.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]):
                #print"coté U.c7 ")
                self.suiteCoupsCpC +=("L D L' ")
                self.l();self.d();self.lp()

            if (self.U.c1.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.B.c3.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.L.c1.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]):
                #print"coté U.c1 ")
                self.suiteCoupsCpC +=("B D B' ")
                self.b();self.d();self.bp()

            # Si D sur oposé
            if (self.D.c1.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.F.c7.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.L.c9.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]):
                #print"coté D.c1 ")
                if(self.F.c7.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("D' ")
                    self.dp()
                elif(self.L.c9.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("D ")
                    self.d()
                elif(self.D.c1.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("D2 ")
                    self.d();self.d()

            # Si sur D sous bon coté
            if (self.D.c9.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.R.c9.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.B.c7.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]):
                #print"coté D.c7 ")
                if(self.R.c9.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("D' ")
                    self.dp()
                elif(self.B.c7.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("D ")
                    self.d()
                elif(self.D.c9.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("R D' R' ")
                    self.r();self.dp();self.rp()

            # Si bord droit
            if (self.D.c7.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.B.c9.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.L.c7.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]):
                #print"coté D.c7 ")
                # Si blanc dessous
                if(self.D.c7.colour == self.U.colourFace):
                    #print"Dessous ")
                    self.suiteCoupsCpC +=("D' ")
                    self.dp()

                # Si blanc mauvais coté
                elif(self.B.c9.colour == self.U.colourFace):
                    #print"Bord droit orienté gauche ")
                    self.suiteCoupsCpC +=("D2 ")
                    self.d();self.d()

                # Si blanc OK
                elif(self.L.c7.colour == self.U.colourFace):
                    #print"Bord droit OK ")
                    self.suiteCoupsCpC +=("R D' R' ")
                    self.r();self.dp();self.rp()

            # Si bord gauche
            if (self.D.c3.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.R.c7.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]\
            and self.F.c9.colour in [self.U.colourFace, self.R.colourFace, self.B.colourFace]):
                #print"coté D.c3 ")
                # Si blanc dessous
                if(self.D.c3.colour == self.U.colourFace):
                    #print"Dessous ")
                    self.suiteCoupsCpC +=("F D F'  ")
                    self.f();self.d();self.fp()

                # Si blanc mauvais coté
                elif(self.R.c7.colour == self.U.colourFace):
                    #print"Bord gauche orienté droit ")
                    self.suiteCoupsCpC +=("D2 ")
                    self.d();self.d()

                # Si blanc OK
                elif(self.F.c9.colour == self.U.colourFace):
                    #print"Bord gauche OK ")
                    self.suiteCoupsCpC +=("B' D B ")
                    self.bp();self.d();self.b()
        # 3e coté
        while (not (self.U.c9.colour == self.U.colourFace\
            and self.F.c3.colour == self.F.colourFace\
            and self.R.c1.colour == self.R.colourFace)):
            #print"\n\ncoté 3 NOK ")

            # Si bon mais inversé
            if (self.U.c9.colour in [self.R.colourFace, self.F.colourFace]\
            and self.F.c3.colour in [self.U.colourFace, self.R.colourFace]\
            and self.R.c1.colour in [self.F.colourFace, self.U.colourFace]):
                # Inversé
                if(self.F.c3.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("F D F' ")
                    self.f();self.d();self.fp()
                elif(self.R.c1.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("R' D R ")
                    self.rp();self.d();self.r()

            # Si sur autre coins
            if (self.U.c7.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.L.c3.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.F.c1.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]):
                #print"coté U.c7 ")
                self.suiteCoupsCpC +=("L D L' ")
                self.l();self.d();self.lp()

            if (self.U.c1.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.B.c3.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.L.c1.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]):
                #print"coté U.c1 ")
                self.suiteCoupsCpC +=("B D B' ")
                self.b();self.d();self.bp()

            if (self.U.c3.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.R.c3.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.B.c1.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]):
                #print"coté U.c3 ")
                self.suiteCoupsCpC +=("R D R' ")
                self.r();self.d();self.rp()

            # Si D sur oposé
            if (self.D.c7.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.L.c7.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.B.c9.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]):
                #print"coté D.c7 ")
                if(self.L.c7.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("D' ")
                    self.dp()
                elif(self.B.c9.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("D ")
                    self.d()
                elif(self.D.c7.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=("D2 ")
                    self.d();self.d()

            # Si sur D sous bon coté
            if (self.D.c3.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.F.c9.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.R.c7.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]):
                #print"coté D.c3 ")
                if(self.F.c9.colour == self.U.colourFace):
                    self.suiteCoupsCpC += ( "D' ")
                    self.dp()
                elif(self.R.c7.colour == self.U.colourFace):
                    self.suiteCoupsCpC += ("D ")
                    self.d()
                elif(self.D.c3.colour == self.U.colourFace):
                    self.suiteCoupsCpC +=( "F D' F' ")
                    self.f();self.dp();self.fp()

            # Si bord gauche
            if (self.D.c1.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.F.c7.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.L.c9.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]):
                #print"coté D.c1 ")
                # Si blanc dessous
                if(self.D.c1.colour == self.U.colourFace):
                    #print"Dessous ")
                    self.suiteCoupsCpC += ( "D ")
                    self.d()

                # Si blanc mauvais coté
                elif(self.F.c7.colour == self.U.colourFace):
                    #print"Bord gauche orienté droit ")
                    self.suiteCoupsCpC += ( "D2 ")
                    self.d();self.d()

                # Si blanc OK
                elif(self.L.c9.colour == self.U.colourFace):
                    #print"Bord gauche OK ")
                    self.suiteCoupsCpC += ( "R' D R ")
                    self.rp();self.d();self.r()

            # Si bord droit
            if (self.D.c9.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.R.c9.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]\
            and self.B.c7.colour in [self.U.colourFace, self.R.colourFace, self.F.colourFace]):
                #print"coté D.c9 ")
                # Si blanc dessous
                if(self.D.c9.colour == self.U.colourFace):
                    #print"Dessous ")
                    self.suiteCoupsCpC += ( "D' ")
                    self.dp()

                # Si blanc mauvais coté
                elif(self.R.c9.colour == self.U.colourFace):
                    #print"Bord droit orienté gauche ")
                    self.suiteCoupsCpC += ( "D2 ")
                    self.d();self.d()

                # Si blanc OK
                elif(self.B.c7.colour == self.U.colourFace):
                    #print"Bord droit OK ")
                    self.suiteCoupsCpC += ( "F D' F' ")
                    self.f();self.dp();self.fp()


        # 4e coins
        while (not (self.U.c7.colour == self.U.colourFace\
            and self.L.c3.colour == self.L.colourFace\
            and self.F.c1.colour == self.F.colourFace)):
            #print"\n\ncoté 4 NOK ")

            # Si bon mais inversé
            if (self.U.c7.colour in [self.L.colourFace, self.F.colourFace]\
            and self.L.c3.colour in [self.U.colourFace, self.F.colourFace]\
            and self.F.c1.colour in [self.L.colourFace, self.U.colourFace]):
                # Inversé
                if(self.L.c3.colour == self.U.colourFace):
                    self.suiteCoupsCpC += ( "L D L' ")
                    self.l();self.d();self.lp()
                elif(self.F.c1.colour == self.U.colourFace):
                    self.suiteCoupsCpC += ( "F' D F ")
                    self.fp();self.d();self.f()

            # Si sur autre coins
            if (self.U.c1.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.B.c3.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.L.c1.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]):
                #print"coté U.c1 ")
                self.suiteCoupsCpC += ( "B D B' ")
                self.b();self.d();self.bp()

            if (self.U.c3.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.R.c3.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.B.c1.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]):
                #print"coté U.c3 ")
                self.suiteCoupsCpC += ( "R D R' ")
                self.r();self.d();self.rp()

            if (self.U.c9.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.F.c3.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.R.c1.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]):
                #print"coté U.c9 ")
                self.suiteCoupsCpC += ( "F D F' ")
                self.f();self.d();self.fp()

            # Si D sur oposé
            if (self.D.c9.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.B.c7.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.R.c9.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]):
                #print"coté D.c9 ")
                if(self.B.c7.colour == self.U.colourFace):
                    self.suiteCoupsCpC += ( "D' ")
                    self.dp()
                elif(self.R.c9.colour == self.U.colourFace):
                    self.suiteCoupsCpC += ( "D ")
                    self.d()
                elif(self.D.c9.colour == self.U.colourFace):
                    self.suiteCoupsCpC += ( "D2 ")
                    self.d();self.d()

            # Si sur D sous bon coté
            if (self.D.c1.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.L.c9.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.F.c7.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]):
                #print"coté D.c1 ")
                if(self.L.c9.colour == self.U.colourFace):
                    self.suiteCoupsCpC += ( "D' ")
                    self.dp()
                elif(self.F.c7.colour == self.U.colourFace):
                    self.suiteCoupsCpC += ( "D ")
                    self.d()
                elif(self.D.c1.colour == self.U.colourFace):
                    self.suiteCoupsCpC += ( "L D' L' ")
                    self.l();self.dp();self.lp()

            # Si bord gauche
            if (self.D.c7.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.L.c7.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.B.c9.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]):
                #print"coté D.c7 ")
                # Si blanc dessous
                if(self.D.c7.colour == self.U.colourFace):
                    #print"Dessous ")
                    self.suiteCoupsCpC += ( "D ")
                    self.d()

                # Si blanc mauvais coté
                elif(self.L.c7.colour == self.U.colourFace):
                    #print"Bord gauche orienté droit ")
                    self.suiteCoupsCpC += ( "D2 ")
                    self.d();self.d()

                # Si blanc OK
                elif(self.B.c9.colour == self.U.colourFace):
                    #print"Bord gauche OK ")
                    self.suiteCoupsCpC += ( "F' D F ")
                    self.fp();self.d();self.f()

            # Si bord droit
            if (self.D.c3.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.F.c9.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]\
            and self.R.c7.colour in [self.U.colourFace, self.L.colourFace, self.F.colourFace]):
                #print"coté D.c3 ")
                # Si blanc dessous
                if(self.D.c3.colour == self.U.colourFace):
                    #print"Dessous ")
                    self.suiteCoupsCpC += ( "D' ")
                    self.dp()

                # Si blanc mauvais coté
                elif(self.F.c9.colour == self.U.colourFace):
                    #print"Bord droit orienté gauche ")
                    self.suiteCoupsCpC += ( "D2 ")
                    self.d();self.d()

                # Si blanc OK
                elif(self.R.c7.colour == self.U.colourFace):
                    #print"Bord droit OK ")
                    self.suiteCoupsCpC += ( "L D' L' ")
                    self.l();self.dp();self.lp()

    # Croix blanche
    def step1(self):
        #print("\nDébut de l'étape 1 : Croix blanche")
        if (not self.croixBlancheOK()):
            self.croixBlanche()

    def croixBlancheOK(self):
        return (self.U.c2.colour == self.U.colourFace and self.U.c4.colour == self.U.colourFace\
            and self.U.c6.colour == self.U.colourFace and self.U.c8.colour == self.U.colourFace\
            and self.B.c2.colour == self.B.colourFace and self.R.c2.colour == self.R.colourFace\
            and self.F.c2.colour == self.F.colourFace and self.L.c2.colour == self.L.colourFace)

    def croixBlanche(self):

        while (self.U.c2.colour != self.U.colourFace or self.B.c2.colour != self.B.colourFace) :
            # Placement de la première arête
            ##print"c2 mal placé ")
            # Si face complémentaire ou adjacent mais inversé
            if self.B.c2.colour == self.U.colourFace and self.U.c2.colour == self.B.colourFace:
                self.suiteCoupsCpC += "B' U R' U' "
                self.bp();self.u();self.rp();self.up()
            if (self.B.c6.colour == self.U.colourFace and self.L.c4.colour == self.B.colourFace) \
                or (self.L.c4.colour == self.U.colourFace and self.B.c6.colour == self.B.colourFace):
                self.suiteCoupsCpC += ("B ")
                self.b()
            if (self.B.c4.colour == self.U.colourFace and self.R.c6.colour == self.B.colourFace) \
                or (self.R.c6.colour == self.U.colourFace and self.B.c4.colour == self.B.colourFace):
                self.suiteCoupsCpC += ("B' ")
                self.bp()
            if (self.B.c8.colour == self.U.colourFace and self.D.c8.colour == self.B.colourFace)\
                or (self.D.c8.colour == self.U.colourFace and self.B.c8.colour == self.B.colourFace):
                self.suiteCoupsCpC += ("B2 ")
                self.b();self.b()

            # Si sur la face D
            if (self.D.c4.colour == self.U.colourFace and self.L.c8.colour == self.B.colourFace) \
                or (self.L.c8.colour == self.U.colourFace and self.D.c4.colour == self.B.colourFace):
                self.suiteCoupsCpC += ("D' ")
                self.dp()
            if (self.D.c6.colour == self.U.colourFace and self.R.c8.colour == self.B.colourFace)\
                or (self.R.c8.colour == self.U.colourFace and self.D.c6.colour == self.B.colourFace):
                self.suiteCoupsCpC += ("D ")
                self.d()
            if (self.D.c2.colour == self.U.colourFace and self.F.c8.colour == self.B.colourFace)\
                or (self.F.c8.colour == self.U.colourFace and self.D.c2.colour == self.B.colourFace):
                self.suiteCoupsCpC += ("D2 ")
                self.d();self.d()

            # si sur face opposé à la complémentaire
            if (self.F.c4.colour == self.U.colourFace and self.L.c6.colour == self.B.colourFace)\
                or ( self.L.c6.colour == self.U.colourFace and self.F.c4.colour == self.B.colourFace):
                self.suiteCoupsCpC += ("F' D2 F ")
                self.fp();self.d();self.d();self.f()
            if (self.F.c6.colour == self.U.colourFace and self.R.c4.colour == self.B.colourFace)\
                or (self.R.c4.colour == self.U.colourFace and self.F.c6.colour == self.B.colourFace):
                self.suiteCoupsCpC += ("F D2 F' ")
                self.f();self.d();self.d();self.fp()

            # Si sur la bonne face mais au mauvais endroit
            if (self.U.c4.colour == self.U.colourFace and self.L.c2.colour == self.B.colourFace)\
                or (self.L.c2.colour == self.U.colourFace and self.U.c4.colour == self.B.colourFace):
                self.suiteCoupsCpC += ("L' ")
                self.lp()
            if (self.U.c6.colour == self.U.colourFace and self.R.c2.colour == self.B.colourFace)\
                or (self.R.c2.colour == self.U.colourFace and self.U.c6.colour == self.B.colourFace):
                self.suiteCoupsCpC += ("R' ")
                self.rp()
            if (self.U.c8.colour == self.U.colourFace and self.F.c2.colour == self.B.colourFace)\
                or (self.F.c2.colour == self.U.colourFace and self.U.c8.colour == self.B.colourFace):
                self.suiteCoupsCpC += ("F2 D2 ")
                self.f();self.f();self.d();self.d()

        # Placement de la seconde arête
        while self.U.c4.colour != self.U.colourFace or self.L.c2.colour != self.L.colourFace:
            # Si face complémentaire ou adjacent mais inversé
            if (self.L.c2.colour == self.U.colourFace and self.U.c4.colour == self.L.colourFace):
                self.suiteCoupsCpC += ("L' U B' U' ")
                self.lp();self.u();self.bp();self.up();
            if (self.L.c6.colour == self.U.colourFace and self.F.c4.colour == self.L.colourFace)\
                or (self.F.c4.colour == self.U.colourFace and self.L.c6.colour == self.L.colourFace):
                self.suiteCoupsCpC += ("L ")
                self.l()
            if (self.L.c4.colour == self.U.colourFace and self.B.c6.colour == self.L.colourFace)\
                or (self.B.c6.colour == self.U.colourFace and self.L.c4.colour == self.L.colourFace):
                self.suiteCoupsCpC += ("L' ")
                self.lp()
            if (self.L.c8.colour == self.U.colourFace and self.D.c4.colour == self.L.colourFace)\
                or (self.D.c4.colour == self.U.colourFace and self.L.c8.colour == self.L.colourFace):
                self.suiteCoupsCpC += ("L2 ")
                self.l();self.l()

            # Si sur la face D
            if (self.D.c8.colour == self.U.colourFace and self.B.c8.colour == self.L.colourFace)\
                or (self.B.c8.colour == self.U.colourFace and self.D.c8.colour == self.L.colourFace):
                self.suiteCoupsCpC += ("D ")
                self.d()
            if (self.D.c6.colour == self.U.colourFace and self.R.c8.colour == self.L.colourFace)\
                or (self.R.c8.colour == self.U.colourFace and self.D.c6.colour == self.L.colourFace):
                self.suiteCoupsCpC += ("D2 ")
                self.d();self.d()
            if (self.D.c2.colour == self.U.colourFace and self.F.c8.colour == self.L.colourFace)\
                or (self.F.c8.colour == self.U.colourFace and self.D.c2.colour == self.L.colourFace):
                self.suiteCoupsCpC += ("D' ")
                self.dp()

            # si sur face opposé à la complémentaire
            if (self.R.c4.colour == self.U.colourFace and self.F.c6.colour == self.L.colourFace)\
                or (self.F.c6.colour == self.U.colourFace and self.R.c4.colour == self.L.colourFace):
                self.suiteCoupsCpC += ("R' D2 R ")
                self.rp();self.d();self.d();self.r()

            if (self.R.c6.colour == self.U.colourFace and self.B.c4.colour == self.L.colourFace)\
                or (self.B.c4.colour == self.U.colourFace and self.R.c6.colour == self.L.colourFace):
                self.suiteCoupsCpC += ("R D2 R' ")
                self.r();self.d();self.d();self.rp()

            # Si sur la bonne face mais au mauvais endroit
            if (self.U.c2.colour == self.U.colourFace and self.B.c2.colour == self.L.colourFace)\
                or (self.B.c2.colour == self.U.colourFace and self.U.c2.colour == self.L.colourFace):
                self.suiteCoupsCpC += ("B ")
                self.b()
            if (self.U.c6.colour == self.U.colourFace and self.R.c2.colour == self.L.colourFace)\
                or (self.R.c2.colour == self.U.colourFace and self.U.c6.colour == self.L.colourFace):
                self.suiteCoupsCpC += ("R2 D2 ")
                self.r();self.r();self.d();self.d()
            if (self.U.c8.colour == self.U.colourFace and self.F.c2.colour == self.L.colourFace)\
                or (self.F.c2.colour == self.U.colourFace and self.U.c8.colour == self.L.colourFace):
                self.suiteCoupsCpC += ("F' ")
                self.fp()

        # Placement de la troisième arête
        while (self.U.c6.colour != self.U.c2.colour or self.R.c2.colour != self.R.colourFace) :
            #print("\nc6 mal placé" )
            # Si face complémentaire ou adjacent mais inversé
            if (self.R.c2.colour == self.U.colourFace and self.U.c6.colour == self.R.colourFace):
                self.suiteCoupsCpC += ("R' U F' U' ")
                self.rp();self.u();self.fp();self.up();
            if (self.R.c6.colour == self.U.colourFace and self.B.c4.colour == self.R.colourFace )\
                or (self.B.c4.colour == self.U.colourFace and self.R.c6.colour == self.R.colourFace):
                self.suiteCoupsCpC += ("R' ")
                self.rp()
            if (self.R.c4.colour == self.U.colourFace and self.F.c6.colour == self.R.colourFace )\
                or (self.F.c6.colour == self.U.colourFace and self.R.c4.colour == self.R.colourFace ):
                self.suiteCoupsCpC += ("R ")
                self.r()
            if (self.R.c8.colour == self.U.colourFace and self.D.c6.colour == self.R.colourFace )\
                or (self.D.c6.colour == self.U.colourFace and self.R.c8.colour == self.R.colourFace ):
                self.suiteCoupsCpC += ("R2 ")
                self.r();self.r()

            # Si sur la face D
            if (self.D.c2.colour == self.U.colourFace and self.F.c8.colour == self.R.colourFace )\
                or (self.F.c8.colour == self.U.colourFace and self.D.c2.colour == self.R.colourFace ):
                self.suiteCoupsCpC += ("D ")
                self.d()
            if (self.D.c4.colour == self.U.colourFace and self.L.c8.colour == self.R.colourFace )\
                or (self.L.c8.colour == self.U.colourFace and self.D.c4.colour == self.R.colourFace ):
                self.suiteCoupsCpC += ("D2 ")
                self.d();self.d()
            if (self.D.c8.colour == self.U.colourFace and self.B.c8.colour == self.R.colourFace )\
                or (self.B.c8.colour == self.U.colourFace and self.D.c8.colour == self.R.colourFace ):
                self.suiteCoupsCpC += ("D' ")
                self.dp()

            # si sur face opposé à la complémentaire
            if (self.L.c4.colour == self.U.colourFace and self.B.c6.colour == self.R.colourFace )\
                or (self.B.c6.colour == self.U.colourFace and self.L.c4.colour == self.R.colourFace ):
                self.suiteCoupsCpC += ("L' D2 L ")
                self.lp();self.d();self.d();self.l()
            if (self.L.c6.colour == self.U.colourFace and self.F.c4.colour == self.R.colourFace )\
                or (self.F.c4.colour == self.U.colourFace and self.L.c6.colour == self.R.colourFace ):
                self.suiteCoupsCpC += ("L D2 L' ")
                self.l();self.d();self.d();self.lp()

            # Si sur la bonne face mais au mauvais endroit
            if (self.U.c2.colour == self.U.colourFace and self.B.c2.colour == self.R.colourFace )\
                or (self.B.c2.colour == self.U.colourFace and self.U.c2.colour == self.R.colourFace ):
                self.suiteCoupsCpC += ("R' ")
                self.rp()
            if (self.U.c4.colour == self.U.colourFace and self.L.c2.colour == self.R.colourFace )\
                or (self.L.c2.colour == self.U.colourFace and self.U.c4.colour == self.R.colourFace ):
                self.suiteCoupsCpC += ("L2 D2 ")
                self.l();self.l();self.d();self.d()
            if (self.U.c8.colour == self.U.colourFace and self.F.c2.colour == self.R.colourFace )\
                or (self.F.c2.colour == self.U.colourFace and self.U.c8.colour == self.R.colourFace ):
                self.suiteCoupsCpC += ("F ")
                self.f()

        # Placement de la dernière arête
        while self.U.c8.colour != self.U.colourFace or self.F.c2.colour != self.F.colourFace:
            #print("\nc8 mal placé")
            # Si face complémentaire ou adjacent mais inversé
            if self.F.c2.colour == self.U.colourFace and self.U.c8.colour == self.F.colourFace:
                self.suiteCoupsCpC += ("F' U L' U' ")
                self.fp();self.u();self.lp();self.up();
            if (self.F.c6.colour == self.U.colourFace and self.R.c4.colour == self.F.colourFace)\
                or (self.R.c4.colour == self.U.colourFace and self.F.c6.colour == self.F.colourFace):
                self.suiteCoupsCpC += ("F' ")
                self.fp()
            if (self.F.c4.colour == self.U.colourFace and self.L.c6.colour == self.F.colourFace)\
                or (self.L.c6.colour == self.U.colourFace and self.F.c4.colour == self.F.colourFace):
                self.suiteCoupsCpC += ("F ")
                self.f()
            if (self.F.c8.colour == self.U.colourFace and self.D.c2.colour == self.F.colourFace)\
                or (self.D.c2.colour == self.U.colourFace and self.F.c8.colour == self.F.colourFace):
                self.suiteCoupsCpC += ("F2 ")
                self.f();self.f()

            # Si sur la face D
            if (self.D.c4.colour == self.U.colourFace and self.L.c8.colour == self.F.colourFace)\
                or (self.L.c8.colour == self.U.colourFace and self.D.c4.colour == self.F.colourFace):
                self.suiteCoupsCpC += ("D ")
                self.d()
            if (self.D.c6.colour == self.U.colourFace and self.R.c8.colour == self.F.colourFace)\
                or (self.R.c8.colour == self.U.colourFace and self.D.c6.colour == self.F.colourFace):
                self.suiteCoupsCpC += ("D' ")
                self.dp()
            if (self.D.c8.colour == self.U.colourFace and self.B.c8.colour == self.F.colourFace)\
                or (self.B.c8.colour == self.U.colourFace and self.D.c8.colour == self.F.colourFace):
                self.suiteCoupsCpC += ("D2 ")
                self.d();self.d()

            # si sur face opposé à la complémentaire
            if (self.B.c4.colour == self.U.colourFace and self.R.c6.colour == self.F.colourFace)\
                or (self.R.c6.colour == self.U.colourFace and self.B.c4.colour == self.F.colourFace):
                self.suiteCoupsCpC += ("B' D2 B ")
                self.bp();self.d();self.d();self.b()
            if (self.B.c6.colour == self.U.colourFace and self.L.c4.colour == self.F.colourFace)\
                or (self.L.c4.colour == self.U.colourFace and self.B.c6.colour == self.F.colourFace):
                self.suiteCoupsCpC += ("B D2 B' ")
                self.b();self.d();self.d();self.bp()

            # Si sur la bonne face mais au mauvais endroit
            if (self.U.c4.colour == self.U.colourFace and self.L.c2.colour == self.F.colourFace)\
                or (self.L.c2.colour == self.U.colourFace and self.U.c4.colour == self.F.colourFace):
                self.suiteCoupsCpC += ("L2 D2 ")
                self.l();self.l();self.d();self.d()
            if (self.U.c6.colour == self.U.colourFace and self.R.c2.colour == self.F.colourFace)\
                or (self.R.c2.colour == self.U.colourFace and self.U.c6.colour == self.F.colourFace):
                self.suiteCoupsCpC += ("R' ")
                self.rp()
            if (self.U.c2.colour == self.U.colourFace and self.B.c2.colour == self.F.colourFace)\
                or (self.B.c2.colour == self.U.colourFace and self.U.c2.colour == self.F.colourFace):
                self.suiteCoupsCpC += ("B' ")
                self.bp()


    #U R2 F B R B2 R U2 L B2 R U' D' R2 F R' L - B2 U2 F2
    def superflip(self):
        self.u(); self.r(); self.r(); self.f(); self.b(); self.r(); self.b(); self.b(); self.r(); self.u(); self.u(); self.l(); \
        self.b(); self.b(); self.r(); self.up(); self.dp(); self.r(); self.r(); self.f(); self.rp(); self.l(); self.b(); \
        self.b(); self.u(); self.u(); self.f(); self.f()

    def cpc(self):

        self.step1()
        self.step2()
        self.step3()
        self.step4()
        self.step5()
        self.step6()
        self.step7()

        #print("\n")

        #print(self.suiteCoupsCpC+"\n")
        save = self.suiteCoupsCpC
        tempo = ""
        while tempo != save :
            self.simplifierCoupsCpc()
            tempo = save
            save = self.suiteCoupsCpC

        return self.suiteCoupsCpC




    def simplifierCoupsCpc(self):
#        tabSuiteCoupsCpC = "A B B C C C D D D D E E E E E "; # OK
#        tabSuiteCoupsCpC = "A2 A B2 B' C2 C2"; # OK
#        tabSuiteCoupsCpC = "A A2 A B B2 B'"; # OK
#        tabSuiteCoupsCpC = "A' B' B' C' C' C' D' D' D' D' E' E' E' E' E'"; #OK
#        print(tabSuiteCoupsCpC+"\n")

        tabSuiteCoupsCpC = self.suiteCoupsCpC;
        tabSuiteCoupsCpC = tabSuiteCoupsCpC.split();

        newSuite = ''
        lastChar = ' '
        nbChar = 0
        nbMove = 0

        for i in range(len(tabSuiteCoupsCpC)):
            # le nombre de mouvement effectué par l'action courant (-1 0 1 ou 2 si X' new X X2)
            nbMove = self.nbMove(lastChar[0], tabSuiteCoupsCpC[i])
            #print(tabSuiteCoupsCpC[i] + " " + lastChar + " " + str(nbMove))

            if nbMove != 0 :
                nbChar += nbMove

            else:
                #print(lastChar + " " + str(nbChar))
                newSuite += self.move(lastChar[0], nbChar)
                nbChar = self.nbMove(tabSuiteCoupsCpC[i][0], tabSuiteCoupsCpC[i])

            lastChar = tabSuiteCoupsCpC[i]

        self.suiteCoupsCpC = newSuite+self.move(lastChar[0], nbChar)

    def nbMove(self, currentChar, newChar):
        # Si égaux
        if currentChar == newChar :
            return 1
        else:
            # Si l'instruction est X2 ou X'
            if len(newChar) == 2:

                # Si le X est lastChar
                if newChar[0] == currentChar:

                    # Si X2
                    if  newChar[1] == '2':
                        return 2

                    # Si X'
                    # Hell Yeah
                    else :
                        return -1

        return 0

    def move(self, char, nbChar):
        pos = True
        if nbChar < 0:
            pos = False
            nbChar *= -1


        if nbChar % 4 == 0:
            return ''
        elif nbChar % 4 == 1:
            if pos:
                return char+' '
            else:
                return char+"' "
        elif nbChar % 4 == 2:
            return char[0]+'2 '
        elif nbChar % 4 == 3:
            if pos:
                return char+"' "
            else:
                return char+" "


def main():
    cube = "LUFFUURRRUBDDRBUUBBBBBFFUBDRLLDDFLUDFLDRLDFRFRLDLBFLRU"
    monCube = CubeSolveur(cube)
    print(monCube.isValide())

if __name__ == "__main__":
    main()
