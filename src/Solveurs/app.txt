solveur --shuffle
	-> renvoie le string du cube melange et la suite de coups correspondant

solveur --stats
	-> cree le fichier de statistiques sur les methodes de resolution

solveur --generateBenchmark X
		-> génére un benchmark de x cubes

solveur --solves cubeString
	-> Resout le cube 3x3 avec toutes les methodes dispo

solveur --solvesn cubeString
	-> Resout le cube NxN avec une seule methode, de facon opti


solveur --solve methode cube
	-> Resout le cube a l'aide de la methode methode


Exemple :

python app.py --generateBenchmark 10

python app.py --stats

python app.py --solves BLFUUFLURBLRDRULRDUFDBFLUDDLFFUDBFFLRBFDLDDRBUBULBRBRR


