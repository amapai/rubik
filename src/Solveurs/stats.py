import time
import kociemba
from cpc3 import *
import sys
import os
sys.path.append(os.getcwd()+'/KorfMethode')
sys.path.append(os.getcwd()+'/Solveurs/KorfMethode')
sys.path.append(os.getcwd()+'/../KorfMethode')
from RubixcubeSolution import *

def stat_Kociemba():
    tpsMoyen = 0.0
    nbCoupsMoyen = 0.0
    nbLine = 0
    fichier = open("benchmark.txt", "r")
    benchmark = fichier.read().split('\n')
    for line in benchmark:
        if len(line)!=0:
            nbLine+=1
            #print(line)
            tps1 = time.time()
            nbCoups = len(kociemba.solve(line).split(" "))
            tps2 = time.time()

            nbCoupsMoyen = (nbCoupsMoyen*nbLine + nbCoups)/(nbLine+1)
            tpsMoyen = (tpsMoyen*nbLine + (tps2-tps1))/(nbLine+1)
    fichier.close()
    print("nbCoups - "+str(nbCoupsMoyen))
    print("tpsMoyen - "+str(tpsMoyen))

    fichier = open("stats.txt", "a")
    fichier.write("Kociemba : \n")
    fichier.write("Nombre de coups en moyenne : "+str(nbCoupsMoyen)+"\n")
    fichier.write("Temps moyen de recherche de solution : "+str(tpsMoyen)+"\n")

def stat_cpc():
    tpsMoyen = 0.0
    nbCoupsMoyen = 0.0
    nbLine = 0
    fichier = open("benchmark.txt", "r")
    benchmark = fichier.read().split('\n')
    for line in benchmark:
        if len(line)!=0:
            nbLine+=1
            #print(line)
            monCube = CubeSolveur(line)
            tps1 = time.time()
            nbCoups = len(monCube.cpc().split(" "))
            tps2 = time.time()

            nbCoupsMoyen = (nbCoupsMoyen*nbLine + nbCoups)/(nbLine+1)
            tpsMoyen = (tpsMoyen*nbLine + (tps2-tps1))/(nbLine+1)
    fichier.close()
    print("nbCoups - "+str(nbCoupsMoyen))
    print("tpsMoyen - "+str(tpsMoyen))

    fichier = open("stats.txt", "a")
    fichier.write("cpc : \n")
    fichier.write("Nombre de coups en moyenne : "+str(nbCoupsMoyen)+"\n")
    fichier.write("Temps moyen de recherche de solution : "+str(tpsMoyen)+"\n")

def stat_Korf():
    tpsMoyen = 0.0
    nbCoupsMoyen = 0.0
    nbLine = 0
    fichier = open("benchmark.txt", "r")
    benchmark = fichier.read().split('\n')
    for line in benchmark:
        if len(line)!=0:
            nbLine+=1
            #print(line)
            tps1 = time.time()
            nbCoups = len(idaLaunch(line).split(" "))
            tps2 = time.time()

            nbCoupsMoyen = (nbCoupsMoyen*nbLine + nbCoups)/(nbLine+1)
            tpsMoyen = (tpsMoyen*nbLine + (tps2-tps1))/(nbLine+1)
    fichier.close()
    print("nbCoups - "+str(nbCoupsMoyen))
    print("tpsMoyen - "+str(tpsMoyen))

    fichier = open("stats.txt", "a")
    fichier.write("cpc : \n")
    fichier.write("Nombre de coups en moyenne : "+str(nbCoupsMoyen)+"\n")
    fichier.write("Temps moyen de recherche de solution : "+str(tpsMoyen)+"\n")

def stats():
    fichier = open("stats.txt", "w")
    fichier.write("")
    stat_Kociemba()
    fichier.write("\n")
    stat_cpc()

def main():
    stat_Korf()


if __name__ == "__main__":
    main()
