import sys
import os
import kociemba
from generateur import *
from stats import *
from cpc3 import *
sys.path.append(os.getcwd()+'/KorfMethode')
sys.path.append(os.getcwd()+'/Solveurs/KorfMethode')
sys.path.append(os.getcwd()+'/../KorfMethode')
from RubixcubeSolution import *

def mauvaiseCMD():
    print(open('app.txt').read())


def main():

    if len(sys.argv) > 1:

        #Un argument
        if len(sys.argv) == 2:

            #Cas demande de cube aleatoire
            if sys.argv[1] == "--shuffle" :
                monCube = Cube("UUUUUUUUURRRRRRRRRFFFFFFFFFDDDDDDDDDLLLLLLLLLBBBBBBBBB")
                monCube.shuffle()
                monCube.toStringShort()
                monCube.toStringSuiteCoupsMelange()

            #Cas demande generation fichier de statistiques sur les algos
            elif sys.argv[1] == "--stats" :
                stats()

            #Cas par defaut
            else :
                print("Commande non reconnue (1 argument)")
                mauvaiseCMD()


        #2 arguments
        elif len(sys.argv) == 3 :

            #Cas generation du benchmark
            if sys.argv[1] == "--generateBenchmark" :
                x = int(sys.argv[2])
                fichier = open("benchmark.txt", "w")
                monCube = Cube("UUUUUUUUURRRRRRRRRFFFFFFFFFDDDDDDDDDLLLLLLLLLBBBBBBBBB")
                for i in range (0, x):
                    monCube.setState("UUUUUUUUURRRRRRRRRFFFFFFFFFDDDDDDDDDLLLLLLLLLBBBBBBBBB")
                    monCube.shuffle()
                    fichier.write(monCube.toStringShort()+"\n")
                fichier.close()

            #Cas solveur avec toutes les methodes
            elif sys.argv[1] == "--solves" :
                cube = sys.argv[2]
                print("Je resous le cube "+cube+" avec toutes les methodes")
                print("Kociemba")
                os.system("./rubiks-cube-NxNxN-solver/usr/bin/rubiks-cube-solver.py --state "+cube+" 2>/dev/null | grep 'Solution' | cut -b 11-")
                print("methode couche par couche")
                monCube = CubeSolveur(cube)
                print(monCube.cpc())
                print("methode korf")
                ret = idaLaunch(cube)
                if(ret == ""):
                    print("timeout")
                else:
                    print(ret)

            #Cas solveur nxn
            elif sys.argv[1] == "--solvesn" :
                cube = sys.argv[2]
                #print("Je resous le cube de taille n")
                os.system("../rubiks-cube-NxNxN-solver/usr/bin/rubiks-cube-solver.py --state "+cube+" 2>/dev/null | grep 'Solution' | cut -b 11-")


            elif sys.argv[1] == "--verify" :
                cube = sys.argv[2]
                if(len(cube) != 54):
                    print(False)
                else:
                    monCube = CubeSolveur(cube)
                    print(monCube.isValide())

            #default
            else :
                print("Commmande non reconnue (2 parametres)")
                mauvaiseCMD()

        elif len(sys.argv) == 4 :
            #Cas solveur unitaire
            if sys.argv[1] == "--solve" :
                methode = sys.argv[2]
                cube = sys.argv[3]
                if methode=="kociemba":
                    os.system("../rubiks-cube-NxNxN-solver/usr/bin/rubiks-cube-solver.py --state "+cube+" 2>/dev/null | grep 'Solution' | cut -b 11-")
                elif methode=="cpc":
                    monCube = CubeSolveur(cube)
                    print(monCube.cpc())
                elif methode=="korf":
                    ret = idaLaunch(cube)
                    if(ret == ""):
                        print("timeout")
                    else:
                        print(ret)

                else:
                    print("methode non reconnue (--solve)")

    else:
        print("Commande non reconnue (3 parametres)")
        mauvaiseCMD()

def requete(methode, cube):
    if methode == "kociemba":
        os.system("../rubiks-cube-NxNxN-solver/usr/bin/rubiks-cube-solver.py --state "+cube+" 2>/dev/null | grep 'Solution' | cut -b 11- > solution.txt")
        fd = open('solution.txt', 'r')
        sol = fd.read()
        fd.close()
        return sol
    elif methode == "cpc":
    	monCube = CubeSolveur(cube)
    	return monCube.cpc()
    elif methode == "korf":
    	ret = idaLaunch(cube)
	if(ret == ""):
		return "timeout"
	else:
		return ret
    elif methode == "n":
		os.system("../rubiks-cube-NxNxN-solver/usr/bin/rubiks-cube-solver.py --state "+cube+" 2>/dev/null | grep 'Solution' | cut -b 11- > solution.txt")
		fd = open('solution.txt', 'r')
		sol = fd.read()
		fd.close()
		return sol

if __name__ == "__main__":
    main()
