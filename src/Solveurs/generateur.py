import random
import sys
import os
sys.path.append(os.getcwd()+'/verifyBiblio')
sys.path.append(os.getcwd()+'/Solveurs/verifyBiblio')
sys.path.append(os.getcwd()+'/../verifyBiblio')
from tools import *


class Carre:

    def __init__(self, colour):
        self.colour = colour

class Face:

    def __init__(self, colours):
        self.colours = colours
        self.colourFace = colours[4]
        self.c1 = Carre(colours[0])
        self.c2 = Carre(colours[1])
        self.c3 = Carre(colours[2])
        self.c4 = Carre(colours[3])
        self.c5 = Carre(colours[4])
        self.c6 = Carre(colours[5])
        self.c7 = Carre(colours[6])
        self.c8 = Carre(colours[7])
        self.c9 = Carre(colours[8])

    def getColours(self):
        colourTmp = ""
        colourTmp += self.c1.colour
        colourTmp += self.c2.colour
        colourTmp += self.c3.colour
        colourTmp += self.c4.colour
        colourTmp += self.c5.colour
        colourTmp += self.c6.colour
        colourTmp += self.c7.colour
        colourTmp += self.c8.colour
        colourTmp += self.c9.colour
        self.colours = colourTmp

    def getColoursSpace(self):
        colourTmp = ""
        colourTmp += self.c1.colour
        colourTmp += self.c2.colour
        colourTmp += self.c3.colour +"\n"
        colourTmp += self.c4.colour
        colourTmp += self.c5.colour
        colourTmp += self.c6.colour +"\n"
        colourTmp += self.c7.colour
        colourTmp += self.c8.colour
        colourTmp += self.c9.colour + "\n" +"\n"
        self.colours = colourTmp

    def resolu(self):
        return not(self.c1.colour != self.colourFace \
        or self.c2.colour != self.colourFace \
        or self.c3.colour != self.colourFace \
        or self.c4.colour != self.colourFace \
        or self.c6.colour != self.colourFace \
        or self.c7.colour != self.colourFace \
        or self.c8.colour != self.colourFace \
        or self.c9.colour != self.colourFace)

class Cube:
#Continent le cube original
#Un cube contient des faces
#Les faces contient des carres
#Le string du cube apres operartion
#La liste des operations
#ORDRE U, R, F, D, L, B

    def __init__(self, colours):
        self.colours = colours

        #TODO gerer
        #print(self.isValide())

        self.suiteCoupsMelange = ""
        self.F = Face(colours[18:27])
        self.D = Face(colours[27:36])
        self.U = Face(colours[0:9])
        self.L = Face(colours[36:45])
        self.R = Face(colours[9:18])
        self.B = Face(colours[45:54])

    def toStringShort(self):
        self.getColours()
        #print(self.colours)
        return self.colours

    def getColours(self):
        coloursTmp = ""
        self.U.getColours()
        coloursTmp += self.U.colours
        self.R.getColours()
        coloursTmp += self.R.colours
        self.F.getColours()
        coloursTmp += self.F.colours
        self.D.getColours()
        coloursTmp += self.D.colours
        self.L.getColours()
        coloursTmp += self.L.colours
        self.B.getColours()
        coloursTmp += self.B.colours
        self.colours = coloursTmp

    def toStringSpace(self):
        self.getColoursSpace()
        print(self.colours)
        self.getColours()

    def getColoursSpace(self):
        coloursTmp = ""
        self.U.getColoursSpace()
        coloursTmp += "Voici la face U : \n" + self.U.colours
        self.R.getColoursSpace()
        coloursTmp += "Voici la face R : \n" + self.R.colours
        self.F.getColoursSpace()
        coloursTmp += "Voici la face F : \n" + self.F.colours
        self.D.getColoursSpace()
        coloursTmp += "Voici la face D : \n" + self.D.colours
        self.L.getColoursSpace()
        coloursTmp += "Voici la face L : \n" + self.L.colours
        self.B.getColoursSpace()
        coloursTmp += "Voici la face B : \n" + self.B.colours
        self.colours = coloursTmp


    def setState(self, newColours):
        self.colours = newColours
        self.suiteCoupsMelange = ""
        self.F = Face(newColours[18:27])
        self.D = Face(newColours[27:36])
        self.U = Face(newColours[0:9])
        self.L = Face(newColours[36:45])
        self.R = Face(newColours[9:18])
        self.B = Face(newColours[45:54])


    def toStringSuiteCoupsMelange(self):
        print(self.suiteCoupsMelange)

    def r(self):
        #Rotation d'un quart de tour de la face de droite dans le sens horaire
        #U devient F, F devient D, D devient B, B devient U (indices 3,6,9 - sauf B 1,4,7)
        #On stocke U
        cubeTmp1 = self.U.c3
        cubeTmp2 = self.U.c6
        cubeTmp3 = self.U.c9
        #U devient F
        self.U.c3 = self.F.c3
        self.U.c6 = self.F.c6
        self.U.c9 = self.F.c9
        #F devient D
        self.F.c3 = self.D.c3
        self.F.c6 = self.D.c6
        self.F.c9 = self.D.c9
        #D devient B
        self.D.c3 = self.B.c7
        self.D.c6 = self.B.c4
        self.D.c9 = self.B.c1
        #B devient U
        self.B.c7 = cubeTmp1
        self.B.c4 = cubeTmp2
        self.B.c1 = cubeTmp3
        #Changement de la face R
        #On stock R.c1 et on s'occupe des angles
        cubeTmp = self.R.c1
        self.R.c1 = self.R.c7
        self.R.c7 = self.R.c9
        self.R.c9 = self.R.c3
        self.R.c3 = cubeTmp
        #On stocke R.c4 et on s'occupe des centres
        cubeTmp = self.R.c4
        self.R.c4 = self.R.c8
        self.R.c8 = self.R.c6
        self.R.c6 = self.R.c2
        self.R.c2 = cubeTmp

    def rp(self):
        #Rotation d'un quart de tour de la face de droite dans le sens anti-horaire
        #F devient U, U devient B, B devient D, D devient F (indices 3,6,9 - B 1,4,7)
        #On stocke U
        cubeTmp1 = self.U.c3
        cubeTmp2 = self.U.c6
        cubeTmp3 = self.U.c9
        #U devient B
        self.U.c3 = self.B.c7
        self.U.c6 = self.B.c4
        self.U.c9 = self.B.c1
        #B devient D
        self.B.c7 = self.D.c3
        self.B.c4 = self.D.c6
        self.B.c1 = self.D.c9
        #D devient F
        self.D.c3 = self.F.c3
        self.D.c6 = self.F.c6
        self.D.c9 = self.F.c9
        #F devient U
        self.F.c3 = cubeTmp1
        self.F.c6 = cubeTmp2
        self.F.c9 = cubeTmp3
        #Changement de la face R
        #On stock R.c1 et on s'occupe des angles
        cubeTmp = self.R.c1
        self.R.c1 = self.R.c3
        self.R.c3 = self.R.c9
        self.R.c9 = self.R.c7
        self.R.c7 = cubeTmp
        #On stocke R.c4 et on s'occupe des centres
        cubeTmp = self.R.c4
        self.R.c4 = self.R.c2
        self.R.c2 = self.R.c6
        self.R.c6 = self.R.c8
        self.R.c8 = cubeTmp

    def l(self):
        #Rotation d'un quart de tour de la face de gauche dans le sens horaire
        #U devient B, B devient D, D devient F, F devient U (indices 1,4,7 - B 3,6,9)
        #On stocke U
        cubeTmp1 = self.U.c1
        cubeTmp2 = self.U.c4
        cubeTmp3 = self.U.c7
        #U devient B
        self.U.c1 = self.B.c9
        self.U.c4 = self.B.c6
        self.U.c7 = self.B.c3
        #B devient D
        self.B.c9 = self.D.c1
        self.B.c6 = self.D.c4
        self.B.c3 = self.D.c7
        #D devient F
        self.D.c1 = self.F.c1
        self.D.c4 = self.F.c4
        self.D.c7 = self.F.c7
        #F devient U
        self.F.c1 = cubeTmp1
        self.F.c4 = cubeTmp2
        self.F.c7 = cubeTmp3
        #Changement de la face L
        #On stock L.c1 et on s'occupe des angles
        cubeTmp = self.L.c1
        self.L.c1 = self.L.c7
        self.L.c7 = self.L.c9
        self.L.c9 = self.L.c3
        self.L.c3 = cubeTmp
        #On stocke L.c4 et on s'occupe des centres
        cubeTmp = self.L.c4
        self.L.c4 = self.L.c8
        self.L.c8 = self.L.c6
        self.L.c6 = self.L.c2
        self.L.c2 = cubeTmp

    def lp(self):
        #Rotation d'un quart de tour de la face de gauche dans le sens anti-horaire
        #U devient F, F devient D, D devient B, B devient U (indices 1,4,7 - B 3,6,9)
        #On stocke U
        cubeTmp1 = self.U.c1
        cubeTmp2 = self.U.c4
        cubeTmp3 = self.U.c7
        #U devient F
        self.U.c1 = self.F.c1
        self.U.c4 = self.F.c4
        self.U.c7 = self.F.c7
        #F devient D
        self.F.c1 = self.D.c1
        self.F.c4 = self.D.c4
        self.F.c7 = self.D.c7
        #D devient B
        self.D.c1 = self.B.c9
        self.D.c4 = self.B.c6
        self.D.c7 = self.B.c3
        #B devient U
        self.B.c9 = cubeTmp1
        self.B.c6 = cubeTmp2
        self.B.c3 = cubeTmp3
        #Changement de la face L
        #On stock L.c1 et on s'occupe des angles
        cubeTmp = self.L.c1
        self.L.c1 = self.L.c3
        self.L.c3 = self.L.c9
        self.L.c9 = self.L.c7
        self.L.c7 = cubeTmp
        #On stocke L.c4 et on s'occupe des centres
        cubeTmp = self.L.c4
        self.L.c4 = self.L.c2
        self.L.c2 = self.L.c6
        self.L.c6 = self.L.c8
        self.L.c8 = cubeTmp


    def u(self):
        #Rotation d'un quart de tour de la face du dessus dans le sens horaire
        #L devient F, F devient R, R devient B, B devient L (indices 1,2,3)
        #On stocke L
        cubeTmp1 = self.L.c1
        cubeTmp2 = self.L.c2
        cubeTmp3 = self.L.c3
        #L devient F
        self.L.c1 = self.F.c1
        self.L.c2 = self.F.c2
        self.L.c3 = self.F.c3
        #F devient R
        self.F.c1 = self.R.c1
        self.F.c2 = self.R.c2
        self.F.c3 = self.R.c3
        #R devient B
        self.R.c1 = self.B.c1
        self.R.c2 = self.B.c2
        self.R.c3 = self.B.c3
        #B devient L
        self.B.c1 = cubeTmp1
        self.B.c2 = cubeTmp2
        self.B.c3 = cubeTmp3
        #Changement de la face U
        #On stock U.c1 et on s'occupe des angles
        cubeTmp = self.U.c1
        self.U.c1 = self.U.c7
        self.U.c7 = self.U.c9
        self.U.c9 = self.U.c3
        self.U.c3 = cubeTmp
        #On stocke U.c4 et on s'occupe des centres
        cubeTmp = self.U.c4
        self.U.c4 = self.U.c8
        self.U.c8 = self.U.c6
        self.U.c6 = self.U.c2
        self.U.c2 = cubeTmp

    def up(self):
        #Rotation d'un quart de tour de la face du dessus dans le sens anti-horaire
        #R devient F, F devient L, L devient B, B devient R (indices 1,2,3)
        #On stocke L
        cubeTmp1 = self.L.c1
        cubeTmp2 = self.L.c2
        cubeTmp3 = self.L.c3
        #L devient B
        self.L.c1 = self.B.c1
        self.L.c2 = self.B.c2
        self.L.c3 = self.B.c3
        #B devient R
        self.B.c1 = self.R.c1
        self.B.c2 = self.R.c2
        self.B.c3 = self.R.c3
        #R devient F
        self.R.c1 = self.F.c1
        self.R.c2 = self.F.c2
        self.R.c3 = self.F.c3
        #F devient L
        self.F.c1 = cubeTmp1
        self.F.c2 = cubeTmp2
        self.F.c3 = cubeTmp3
        #Changement de la face U
        #On stock U.c1 et on s'occupe des angles
        cubeTmp = self.U.c1
        self.U.c1 = self.U.c3
        self.U.c3 = self.U.c9
        self.U.c9 = self.U.c7
        self.U.c7 = cubeTmp
        #On stocke U.c4 et on s'occupe des centres
        cubeTmp = self.U.c4
        self.U.c4 = self.U.c2
        self.U.c2 = self.U.c6
        self.U.c6 = self.U.c8
        self.U.c8 = cubeTmp

    def d(self):
        #Rotation d'un quart de tour de la face du dessous dans le sens horaire
        #L devient B, B devient R, R devient F, F devient L (indices 7,8,9)
        #On stocke L
        cubeTmp1 = self.L.c7
        cubeTmp2 = self.L.c8
        cubeTmp3 = self.L.c9
        #L devient B
        self.L.c7 = self.B.c7
        self.L.c8 = self.B.c8
        self.L.c9 = self.B.c9
        #B devient R
        self.B.c7 = self.R.c7
        self.B.c8 = self.R.c8
        self.B.c9 = self.R.c9
        #R devient F
        self.R.c7 = self.F.c7
        self.R.c8 = self.F.c8
        self.R.c9 = self.F.c9
        #F devient L
        self.F.c7 = cubeTmp1
        self.F.c8 = cubeTmp2
        self.F.c9 = cubeTmp3
        #Changement de la face L
        #On stock D.c1 et on s'occupe des angles
        cubeTmp = self.D.c1
        self.D.c1 = self.D.c7
        self.D.c7 = self.D.c9
        self.D.c9 = self.D.c3
        self.D.c3 = cubeTmp
        #On stocke D.c4 et on s'occupe des centres
        cubeTmp = self.D.c4
        self.D.c4 = self.D.c8
        self.D.c8 = self.D.c6
        self.D.c6 = self.D.c2
        self.D.c2 = cubeTmp

    def dp(self):
        #Rotation d'un quart de tour de la face du dessous dans le sens anti-horaire
        #L devient F, F devient R, R devient B, B devient L (indices 7,8,9)
        #On stocke L
        cubeTmp1 = self.L.c7
        cubeTmp2 = self.L.c8
        cubeTmp3 = self.L.c9
        #L devient F
        self.L.c7 = self.F.c7
        self.L.c8 = self.F.c8
        self.L.c9 = self.F.c9
        #F devient R
        self.F.c7 = self.R.c7
        self.F.c8 = self.R.c8
        self.F.c9 = self.R.c9
        #R devient B
        self.R.c7 = self.B.c7
        self.R.c8 = self.B.c8
        self.R.c9 = self.B.c9
        #B devient L
        self.B.c7 = cubeTmp1
        self.B.c8 = cubeTmp2
        self.B.c9 = cubeTmp3
        #Changement de la face D
        #On stock D.c1 et on s'occupe des angles
        cubeTmp = self.D.c1
        self.D.c1 = self.D.c3
        self.D.c3 = self.D.c9
        self.D.c9 = self.D.c7
        self.D.c7 = cubeTmp
        #On stocke D.c4 et on s'occupe des centres
        cubeTmp = self.D.c4
        self.D.c4 = self.D.c2
        self.D.c2 = self.D.c6
        self.D.c6 = self.D.c8
        self.D.c8 = cubeTmp

    def f(self):
        #Rotation d'un quart de tour de la face avant dans le sens horaire
        #U devient L, L devient D, D devient R et R devient U
        #On stocke U
        cubeTmp1 = self.U.c7
        cubeTmp2 = self.U.c8
        cubeTmp3 = self.U.c9
        #U devient L
        self.U.c7 = self.L.c9
        self.U.c8 = self.L.c6
        self.U.c9 = self.L.c3
        #L devient D
        self.L.c3 = self.D.c1
        self.L.c6 = self.D.c2
        self.L.c9 = self.D.c3
        #D devient R
        self.D.c1 = self.R.c7
        self.D.c2 = self.R.c4
        self.D.c3 = self.R.c1
        #R devient U
        self.R.c1 = cubeTmp1
        self.R.c4 = cubeTmp2
        self.R.c7 = cubeTmp3
        #On reste sur la face F avec permutaion des indices :
        #1,2,3 -> 7,4,1 - 1,4,7 -> 7,8,9 - 7,8,9 -> 8,6,3 - 3,6,9 -> 1,2,3
        #On stock F.c1 et on s'occupe des angles
        cubeTmp = self.F.c1
        self.F.c1 = self.F.c7
        self.F.c7 = self.F.c9
        self.F.c9 = self.F.c3
        self.F.c3 = cubeTmp
        #On stocke F.c4 et on s'occupe des centres
        cubeTmp = self.F.c4
        self.F.c4 = self.F.c8
        self.F.c8 = self.F.c6
        self.F.c6 = self.F.c2
        self.F.c2 = cubeTmp

    def fp(self):
        #Rotation d'un quart de tour de la face avant dans le sens anti-horaire
        #U devient R, R devient D, D devient L et L devient U
        #On stocke U
        cubeTmp1 = self.U.c7
        cubeTmp2 = self.U.c8
        cubeTmp3 = self.U.c9
        #U devient R
        self.U.c7 = self.R.c1
        self.U.c8 = self.R.c4
        self.U.c9 = self.R.c7
        #R devient D
        self.R.c1 = self.D.c3
        self.R.c4 = self.D.c2
        self.R.c7 = self.D.c1
        #D devient L
        self.D.c1 = self.L.c3
        self.D.c2 = self.L.c6
        self.D.c3 = self.L.c9
        #L devient U
        self.L.c9 = cubeTmp1
        self.L.c6 = cubeTmp2
        self.L.c3 = cubeTmp3
        #Changement de la face F
        #On stock F.c1 et on s'occupe des angles
        cubeTmp = self.F.c1
        self.F.c1 = self.F.c3
        self.F.c3 = self.F.c9
        self.F.c9 = self.F.c7
        self.F.c7 = cubeTmp
        #On stocke F.c4 et on s'occupe des centres
        cubeTmp = self.F.c4
        self.F.c4 = self.F.c2
        self.F.c2 = self.F.c6
        self.F.c6 = self.F.c8
        self.F.c8 = cubeTmp

    def b(self):
        #Rotation d'un quart de tour de la face arriere dans le sens horaire
        #U devient R, R devient D, D devient L et L devient U
        #On stocke U
        cubeTmp1 = self.U.c1
        cubeTmp2 = self.U.c2
        cubeTmp3 = self.U.c3
        #U devient R
        self.U.c1 = self.R.c3
        self.U.c2 = self.R.c6
        self.U.c3 = self.R.c9
        #R devient D
        self.R.c3 = self.D.c9
        self.R.c6 = self.D.c8
        self.R.c9 = self.D.c7
        #D devient L
        self.D.c9 = self.L.c7
        self.D.c8 = self.L.c4
        self.D.c7 = self.L.c1
        #L devient U
        self.L.c7 = cubeTmp1
        self.L.c4 = cubeTmp2
        self.L.c1 = cubeTmp3
        #On reste sur la face F avec permutaion des indices :
        #1,2,3 -> 7,4,1 - 1,4,7 -> 7,8,9 - 7,8,9 -> 8,6,3 - 3,6,9 -> 1,2,3
        #On stock F.c1 et on s'occupe des angles
        cubeTmp = self.B.c1
        self.B.c1 = self.B.c7
        self.B.c7 = self.B.c9
        self.B.c9 = self.B.c3
        self.B.c3 = cubeTmp
        #On stocke F.c4 et on s'occupe des centres
        cubeTmp = self.B.c4
        self.B.c4 = self.B.c8
        self.B.c8 = self.B.c6
        self.B.c6 = self.B.c2
        self.B.c2 = cubeTmp

    def bp(self):
        #Rotation d'un quart de tour de la face arriere dans le sens anti-horaire
        #U devient L, L devient D, D devient R et R devient U
        #On stocke U
        cubeTmp1 = self.U.c1
        cubeTmp2 = self.U.c2
        cubeTmp3 = self.U.c3
        #U devient L
        self.U.c1 = self.L.c7
        self.U.c2 = self.L.c4
        self.U.c3 = self.L.c1
        #L devient D
        self.L.c1 = self.D.c7
        self.L.c4 = self.D.c8
        self.L.c7 = self.D.c9
        #D devient R
        self.D.c7 = self.R.c9
        self.D.c8 = self.R.c6
        self.D.c9 = self.R.c3
        #R devient U
        self.R.c3 = cubeTmp1
        self.R.c6 = cubeTmp2
        self.R.c9 = cubeTmp3
        #Changement de la face B
        #On stock B.c1 et on s'occupe des angles
        cubeTmp = self.B.c1
        self.B.c1 = self.B.c3
        self.B.c3 = self.B.c9
        self.B.c9 = self.B.c7
        self.B.c7 = cubeTmp
        #On stocke B.c4 et on s'occupe des centres
        cubeTmp = self.B.c4
        self.B.c4 = self.B.c2
        self.B.c2 = self.B.c6
        self.B.c6 = self.B.c8
        self.B.c8 = cubeTmp

    def shuffle(self):
        #R L U D F B
        for i in range(0,20):
            i = random.randint(0,12)
            if i == 0 :
                self.r()
                self.suiteCoupsMelange += "R "
            elif i == 1:
                self.l()
                self.suiteCoupsMelange += "L "
            elif i == 2:
                self.u()
                self.suiteCoupsMelange += "U "
            elif i == 3:
                self.d()
                self.suiteCoupsMelange += "D "
            elif i == 4:
                self.f()
                self.suiteCoupsMelange += "F "
            elif i==5:
                self.b()
                self.suiteCoupsMelange += "B "
            elif i==6:
                self.rp()
                self.suiteCoupsMelange += "R' "
            elif i==7:
                self.lp()
                self.suiteCoupsMelange += "L' "
            elif i==8:
                self.up()
                self.suiteCoupsMelange += "U' "
            elif i==9:
                self.dp()
                self.suiteCoupsMelange += "D' "
            elif i==10:
                self.fp()
                self.suiteCoupsMelange += "F' "
            else:
                self.bp()
                self.suiteCoupsMelange += "B' "

    #U R2 F B R B2 R U2 L B2 R U' D' R2 F R' L - B2 U2 F2
    def superflip(self):
        self.u(); self.r(); self.r(); self.f(); self.b(); self.r(); self.b(); self.b(); self.r(); self.u(); self.u(); self.l(); \
        self.b(); self.b(); self.r(); self.up(); self.dp(); self.r(); self.r(); self.f(); self.rp(); self.l(); self.b(); \
        self.b(); self.u(); self.u(); self.f(); self.f()

    def isValide(self):
        return verify(self.colours) == 0

        # check : bon nombre de couleurs
        # check : Permutation parity
        # check : Corner parity
        # check : Edge parity
