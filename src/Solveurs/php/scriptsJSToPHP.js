  var path = "http://localhost:8005/";
  /*
   * Contacte le script php qui execute le script python
   * @param methode : kociemba, korf ou cpc
   * @param cube : la représentation du cube : BLFUUFLURBLRDRULRDUFDBFLUDDLFFUDBFFLRBFDLDDRBUBULBRBRR
   * @param callback : la méthode à appeler une fois le script fini
   *
   * contacter3x3("kociemba", stringdeBLBLBLBL, fonction);
   * contacter3x3("korf", stringdeBLBLBLBL, fonction);
   * contacter3x3("cpc", stringdeBLBLBLBL, fonction);
   */
  function contacter3x3(methode, cube, callback){
    //"cpc BLFUUFLURBLRDRULRDUFDBFLUDDLFFUDBFFLRBFDLDDRBUBULBRBRR"
    var params = methode + "/" + cube;
    readPHP(params, callback);
  }

  /*
   * Contacte le script php qui execute le script python
   *
   * @param cube : la représentation du cube : BLFUUFLURBLRDRULRDUFDBFLUDDLFFUDBFFLRBFDLDDRBUBULBRBRR
   * @param callback : la méthode à appeler une fois le script fini
   *
   * contacterNxN(stringdeBLBLBLBL, fonction);
   */
  function contacterNxN(cube, callback){
    //"n BLFUUFLURBLRDRULRDUFDBFLUDDLFFUDBFFLRBFDLDDRBUBULBRBRR"
    var params = "n/" + cube;
    readPHP(params, callback);
  }
  
  /*
   * Permet de lire le script PHP
   * @param file : chemin vers le script
   * @param params : les parametres get utilisé
   * @param callback : la méthode à appeler une fois le script fini
   */
  function readPHP(params, callBack){
    let httpRequest = new XMLHttpRequest();
    httpRequest.open("GET", path+params, true);
    httpRequest.setRequestHeader('Access-Control-Allow-Headers', '*');
    httpRequest.setRequestHeader('Access-Control-Allow-Origin', '*');
    httpRequest.addEventListener("load", function () {
        callBack(httpRequest.responseText); });
    httpRequest.send(null);
  }
