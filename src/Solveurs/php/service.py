#!/usr/bin/env python3

import sys
import os
sys.path.append(os.getcwd()+'/..')

from app import requete

def application(environ, start_response):

	args = environ.get('PATH_INFO').split('/')

	resultat = ''
	
	if(len(args) == 3):
		
		methode = args[1]
		chaine = args[2]

		resultat = requete(methode, chaine)

		start_response('200 OK', [('Access-Control-Allow-Origin', '*'),('Access-Control-Allow-Headers', '*')])

	else :
	
		resultat = 'Pas 2 arguments'
		start_response('200 OK', [])

	return [resultat]
