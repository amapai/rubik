//*************************
//*** Objects variables ***
//*************************

// variable representing webcam image
var capture;

// variabe representing the cube
var cube;
var cubeReference;

// cube dimensions (n * n * n)
var cubeDimension;

// to define colors
var areColorsDefined;

// to only classify once
var isClassified;

// to play animation in draw()
var isAnimationTime; // determine when the animation starts in the app's life.
var currentMove = new Move(0, 0, 0, 0, 0, 0); // initialized the var with stub.
var counter = 0; // used to repeat the animation if over.

//************************
//*** Visual variables ***
//************************

// grid display
var space;
var lineSize;

// color detection
var detectionOffset;
var detectionSquareSize;

//***********************
//*** Audio variables ***
//***********************
var lang = 'fr-FR';
var speechRec;

//************************************************
//*** Setup function, called once at the start ***
//************************************************
function setup() {

    // global display
    let videoWidth = 400;
    let videoHeight = 300;
    let fps = 10;
    let speed = 0.05;
    // creation of the video object
    capture = createCapture(VIDEO);
    capture.size(videoWidth, videoHeight);
    capture.hide();
    currentMove.start();

    // creation of the audio object
    speechRec = new p5.SpeechRec(lang)
    // lancement de l'audio, en continue demandant une pause entre chaque phrase
    speechRec.continuous = true
    speechRec.onResult = gotSpeech
    speechRec.start()

    cubeReference = new CubeColorReference();

    // creation of the canvas
    // this is where p5 objects are displayed
    createCanvas(capture.width, capture.height).parent('camera');

    // maximum number of frame per second
    // draw will be called at most fps times per second
    // better ressource usage
    frameRate(fps);

}

//****************************************
//*** Callback from speech recognition ***
//****************************************
function gotSpeech() {
    if(speechRec.resultValue) {
        console.log(speechRec.resultString);
        if(speechRec.resultString.indexOf('suivant') > -1) {
            let button = document.getElementById('btn-nextFace');
            if(button) {
                button.onclick();
            }
        }/*let button = document.getElementById('btn-next');
            if(button) {
                button.onclick();
            }
        } else if(speechRec.resultString.indexOf('retour') > -1) {
            let button = document.getElementById('btn-previous');
            if(button) {
                button.onclick();
            }
        } else if(speechRec.resultString.indexOf('continue') > -1) {
            let button = document.getElementById('btn-play');
            if(button) {
                button.onclick();
            }
        } else if(speechRec.resultString.indexOf('pause') > -1) {
            let button = document.getElementById('btn-pause');
            if(button) {
                button.onclick();
            }
        }*/
    }
}

//**********************************************************
//*** Draw function, called at most fps times per second ***
//**********************************************************
function draw() {

    // We only need to draw this if cubeDimension and colors are initialized
    if(cubeDimension && !isAnimationTime) {

        // draw background (black or video display)
        if(isClassified) {
            background(0)
        } else {
            // video display
            push()
            translate(width, 0)
            scale(-1.0, 1.0)
            image(capture, 0, 0, width, height);
            pop()
        }

        if(areColorsDefined) {
            fillGrid();
        }

        // draw the grid used to detect colors of the cube
        gridDisplay();
    }
    // Active only when it is animation time
    if(cubeDimension && isAnimationTime){
        playAnimation();
    }
}

//*********************************************************
//*** Function called each time the mouse click is used ***
//*********************************************************
function mouseClicked() {

    // changing clicked-square colors using global selectedColor variable
    for (let i = 0; i < cubeDimension; i++) {
        for (let j = 0; j < cubeDimension; j++) {
            if(cubeDimension % 2 == 0
            || i != floor(cubeDimension / 2)
            || j != floor(cubeDimension / 2)
            ) {
                if(selectedColor
                && mouseX > (i+1) * space + detectionOffset
                && mouseX < (i+2) * space + detectionOffset
                && mouseY > (j+1) * space + detectionOffset
                && mouseY < (j+2) * space + detectionOffset) {
                    cube.getFaceFromLetter(activeFace).squares[j][i] = selectedColor
                }
            }
        }
    }
}

//**************************************
//*** function used to draw the grid ***
//**************************************
function gridDisplay() {

    // side line width
    strokeWeight(lineSize);
    noFill()

    let borders = cube.getFaceFromLetter(activeFace).getBorders()
    let center_color = cube.getFaceFromLetter(activeFace).squareGetColor(activeFace)

    if(!areColorsDefined) {
        strokeWeight(2 * lineSize);
        stroke(center_color)
        rect(
            10 * detectionOffset,
            10 * detectionOffset,
            height - 15 * detectionOffset,
            height - 15 * detectionOffset,
        );
    } else {

        // drawing the lines
        for (let i = 1; i <= cubeDimension + 1; i++) {

            // Haut
            if(i == 1) {
                stroke(borders[0])
            }
            // Bas
            else if(i == cubeDimension + 1) {
                stroke(borders[2])
            }
            // Centre
            else {
                stroke(center_color)
            }

            line(space, i * space, height - space, i * space);

            // Gauche
            if(i == 1) {
                stroke(borders[1])
            }
            // Droite
            else if(i == cubeDimension + 1) {
                stroke(borders[3])
            }
            // Centre
            else {
                stroke(center_color)
            }

            line(i * space, space, i * space, height - space);
        }
    }
}

//***********************************************************
//*** function used to display detected color on the grid ***
//***********************************************************
function fillGrid() {
    let current_face = cube.getFaceFromLetter(activeFace);
    let detected_colors = current_face.squares

    for (let i = 0; i < detected_colors.length; i++) {
        for (let j = 0; j < detected_colors[i].length; j++) {

            square_color = detected_colors[j][i].toString();

            noStroke();

            let color_string = current_face.squareGetColor(square_color);

            if(color_string != "grey") {
                fill(color_string);
            } else {
                noFill();
            }

            rect((i+1) * space + detectionOffset, (j+1) * space + detectionOffset, detectionSquareSize, detectionSquareSize);

        }
    }

}

//*******************************************************************
//*** function calculating mean of RGB values of the current face ***
//*******************************************************************
function initializeFace() {

    if(!areColorsDefined) {

        loadPixels();
        let density = pixelDensity();

        // offset on the pixel array (formula from P5.js website)
        // (lines * line_size + place_in_ligne) * density (1) * 4 (RGBA)
        let offset = (11 * detectionOffset * width + 11 * detectionOffset) * density * 4;
        let calibration_size = height - 16 * detectionOffset

        let mean_r = 0;
        let mean_g = 0;
        let mean_b = 0;

        let number_of_pixels = 0;

        // summing all pixels RGB values
        for (let k = 0; k < calibration_size; k++) {
            for (let l = 0; l < calibration_size; l++) {
                number_of_pixels++
                mean_r += pixels[offset + 4 * (k + (l*width))]
                mean_g += pixels[offset + 4 * (k + (l*width)) + 1]
                mean_b += pixels[offset + 4 * (k + (l*width)) + 2]
            }
        }

        mean_r /= number_of_pixels
        mean_g /= number_of_pixels
        mean_b /= number_of_pixels

        switch (activeFace) {
            case "w":
                cubeReference.meanWhite = new MeanColor(mean_r, mean_g, mean_b);
                break;
            case "r":
                cubeReference.meanRed = new MeanColor(mean_r, mean_g, mean_b);
                break;
            case "b":
                cubeReference.meanBlue = new MeanColor(mean_r, mean_g, mean_b);
                break;
            case "o":
                cubeReference.meanOrange = new MeanColor(mean_r, mean_g, mean_b);
                break;
            case "g":
                cubeReference.meanGreen = new MeanColor(mean_r, mean_g, mean_b);
                break;
            case "y":
                cubeReference.meanYellow = new MeanColor(mean_r, mean_g, mean_b);
                break;
        }

    } else {

        let current_face = cube.getFaceFromLetter(activeFace);

        // only taking picture once
        if(current_face.isInitialized) { return; }
        current_face.isInitialized = true;

        // P5.js function : create an array of rgba code for each pixel
        loadPixels();
        let density = pixelDensity();

        // for each square of the cube
        for (let x = 0; x < cubeDimension; x++) {
            for (let y = 0; y < cubeDimension; y++) {

                // reverse y index because of miror camera display
                let reversed_y = cubeDimension - y - 1
                let current_square = current_face.squares[x][reversed_y];

                // offset on the pixel array (formula from P5.js website)
                // (lines * line_size + place_in_ligne) * density (1) * 4 (RGBA)
                let offset = ((((x+1) * space) + detectionOffset) * width +
                    ((y+1) * space) + detectionOffset) * density * 4;

                // summing all pixels RGB values
                for (let k = 0; k < detectionSquareSize; k++) {
                    for (let l = 0; l < detectionSquareSize; l++) {
                        current_square.meanR += pixels[offset + 4 * (k + (l*width))]
                        current_square.meanG += pixels[offset + 4 * (k + (l*width)) + 1]
                        current_square.meanB += pixels[offset + 4 * (k + (l*width)) + 2]
                    }
                }

                // dividing by number of pixel in the square
                let number_of_pixels = detectionSquareSize * detectionSquareSize;
                current_square.meanR = floor(current_square.meanR / number_of_pixels);
                current_square.meanG = floor(current_square.meanG / number_of_pixels);
                current_square.meanB = floor(current_square.meanB / number_of_pixels);
            }
        }
    }
}

//*******************************************
//*** Classify colors of each face square ***
//*******************************************
function knnColorClassifier() {

    // On ne classifie qu'une seule fois
    if(isClassified) { return; }
    isClassified = true;

    let faces = cube.facesToArray();

    // [ {square_reference, color, similarity} ]
    let squares_similarities = [];

    // pour chaque cubique
    for (let face of faces) {
        for (let row of face.squares) {
            for (let square of row) {
                // S'il n'a pas de couleur
                // Calculer et stocker les différents scores de similarité
                if(square.color == "") {
                    squares_similarities.push({
                        'square_reference' : square,
                        'color' : 'g',
                        'similarity' : square.similarity(cubeReference.meanGreen)
                    });
                    squares_similarities.push({
                        'square_reference' : square,
                        'color' : 'b',
                        'similarity' : square.similarity(cubeReference.meanBlue)
                    });
                    squares_similarities.push({
                        'square_reference' : square,
                        'color' : 'y',
                        'similarity' : square.similarity(cubeReference.meanYellow)
                    });
                    squares_similarities.push({
                        'square_reference' : square,
                        'color' : 'r',
                        'similarity' : square.similarity(cubeReference.meanRed)
                    });
                    squares_similarities.push({
                        'square_reference' : square,
                        'color' : 'o',
                        'similarity' : square.similarity(cubeReference.meanOrange)
                    });
                    squares_similarities.push({
                        'square_reference' : square,
                        'color' : 'w',
                        'similarity' : square.similarity(cubeReference.meanWhite)
                    });
                }
            }
        }
    }

    // Tri du tableau dans l'ordre décroissant
    squares_similarities.sort((squareA, squareB) => {
        return squareB.similarity - squareA.similarity
    });

    let identified_colors = {
        'g' : 0,
        'b' : 0,
        'y' : 0,
        'r' : 0,
        'o' : 0,
        'w' : 0
    }

    let number_of_square_to_classify = (cubeDimension * cubeDimension - (cubeDimension % 2)) * 6
    for (let i = 0; i < number_of_square_to_classify; i++) {
        let actual_item = squares_similarities[0]

        // assignation de la couleur
        actual_item.square_reference.color = actual_item.color

        // incrémentation du compteur correspondant
        identified_colors[actual_item.color]++

        // suppression de la liste pour le cubique venant d'être classifié
        squares_similarities = squares_similarities.filter(obj => obj.square_reference != actual_item.square_reference);

        // si l'on a trouvé un bon nombre de cubiques d'un même couleur, on ignore cette couleur pour la suite
        if(identified_colors[actual_item.color] >= cubeDimension * cubeDimension - (cubeDimension % 2)) {
            squares_similarities = squares_similarities.filter(obj => obj.color != actual_item.color);
        }
    }
}


//******************************************************************************
//************************ ANIMATION FUNCTIONS *********************************
//******************************************************************************

/**
 * Main function for the animation display. Orients the cube for a nice display
 * If the animation is not over, continues it.
 * Otherwise, it stops temporarly and starts again.
 */
function playAnimation(){
    counter++;
    background(0);
    rotateX(HALF_PI-0.4);
    rotateZ(0.6);
    currentMove.update();
    if (currentMove.isFinished()) {
        // animation over, we display the cube then start again
        // apply currentMovement.angle to our cubics, save it somewhere.
        cube.displayCube();
        if (counter > 75) {
            // reset the cube and starts movement again
            currentMove.start();
            counter = 0;
        }
    } else { // animate
        (cube.dimension!=3)?playNxNMove():play3x3Move();
    }
}

/**
 * Animates the 3D cube and play the current movement for
 * 3x3 moves's format only. Update cube coordinates also.
 */
function play3x3Move(){
    for (let i = 0; i < cube.cubics.length; i++) {
        push();
        if (cube.cubics[i].z == currentMove.z+(cube.cubicSize/2)){
            rotateZ(currentMove.angle);
        } else if (cube.cubics[i].x == currentMove.x+(cube.cubicSize/2)) {
            rotateX(currentMove.angle);
        } else if (cube.cubics[i].y == currentMove.y+(cube.cubicSize/2)) {
            rotateY(currentMove.angle);
        }
        cube.cubics[i].display();
        pop();
    }
}

/**
 * Animates the 3D cube by calling the correct animation function depending on
 * the type of movements wanted.
 * We have different functions depending on the movement because... The cube
 * have to act differently depending which movement is being played. In addition
 * it is easier to read ;)
 */
function playNxNMove(){
    switch(currentMove.name){
        case "F":
            playFMove();
            break;
        case "B":
            playBMove();
            break;
        case "L":
            playLMove();
            break;
        case "R":
            playRMove();
            break;
        case "U":
            playUMove();
            break;
        case "D":
            playDMove();
            break;
        default:
            break;
    }
}

/**
 * Animates the 3D cube for a F like move (2F, 3F2,...).
 * For display only and does not touch the cube's coordinates.
 */
function playFMove(){
    let cubic;
    for (let i = 0; i < cube.cubics.length; i++) {
        cubic = cube.cubics[i];
        push();
        if (currentMove.y < 75 && cubic.y >= (currentMove.y+(cube.cubicSize/2)-0.05)){
            rotateY(currentMove.angle);
        }
        cubic.display();
        pop();
    }
}
/**
 * Animates the 3D cube for a B like move (2B, 3B2,...).
 * For display only and does not touch the cube's coordinates.
 */
function playBMove(){
    let cubic;
    for (let i = 0; i < cube.cubics.length; i++) {
        cubic = cube.cubics[i];
        push();
        if (currentMove.y < 75 && (cubic.y) <= (currentMove.y+(cube.cubicSize/2)+0.05)){
           rotateY(currentMove.angle);
       }
        cubic.display();
        pop();
    }
}
/**
 * Animates the 3D cube for a L like move (2L, 3L2,...).
 * For display only and does not touch the cube's coordinates.
 */
function playLMove(){
    let cubic;
    for (let i = 0; i < cube.cubics.length; i++) {
        cubic = cube.cubics[i];
        push();
        if (currentMove.x < 75 && cubic.x <= (currentMove.x+(cube.cubicSize/2)+0.05)) {
            rotateX(currentMove.angle);
        }
       cubic.display();
       pop();
    }
}
/**
 * Animates the 3D cube for a R like move (2R, 3R2,...).
 * For display only and does not touch the cube's coordinates.
 */
function playRMove(){
    let cubic;
    for (let i = 0; i < cube.cubics.length; i++) {
        cubic = cube.cubics[i];
        push();
        if (currentMove.x < 75 && cubic.x >= (currentMove.x+(cube.cubicSize/2)-0.05)) {
            rotateX(currentMove.angle);
        }
       cubic.display();
       pop();
    }
}
/**
 * Animates the 3D cube for a U like move (2U, 3U2,...).
 * For display only and does not touch the cube's coordinates.
 */
function playUMove(){
    let cubic;
    for (let i = 0; i < cube.cubics.length; i++) {
        cubic = cube.cubics[i];
        push();
        if (currentMove.z < 75 && cubic.z >= (currentMove.z+(cube.cubicSize/2)-0.05)) {
            rotateZ(currentMove.angle);
        }
        cubic.display();
        pop();
    }
}
/**
 * Animates the 3D cube for a D like move (2D, 3D2,...).
 * For display only and does not touch the cube's coordinates.
 */
function playDMove(){
    let cubic;
    for (let i = 0; i < cube.cubics.length; i++) {
        cubic = cube.cubics[i];
        push();
        if (currentMove.z < 75 && cubic.z <= (currentMove.z+(cube.cubicSize/2)+0.05)) {
            rotateZ(currentMove.angle);
        }
        cubic.display();
        pop();
    }
}
