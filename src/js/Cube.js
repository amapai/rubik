// size max of the 3D cube
let MAX_DIMENSION_CUBE = 150;
// indice max of the cube in the model
let MAX_COORDONNEE_CUBE = MAX_DIMENSION_CUBE / 2;

function Cube(n) {

   //**************************************************************
   //**************** ATTRIBUTS ***********************************
   //**************************************************************
   this.dimension = n;
   this.cubicSize = (MAX_DIMENSION_CUBE/this.dimension);
   this.cubics = [];

   /* We imagine the cube stay fix. The white face is always at front. */
   /* Rubik's cube's faces */
   this.greenFace = new Face(this.dimension, "g");
   this.orangeFace = new Face(this.dimension, "o");
   this.whiteFace = new Face(this.dimension, "w");
   this.blueFace = new Face(this.dimension, "b");
   this.redFace = new Face(this.dimension, "r");
   this.yellowFace = new Face(this.dimension, "y");

   /** Array that stores the movements solving cube
       Movements possible are : F R U B L D + F' R' U' B' L' D' + NxN's */
   this.movements = [];



   //**************************************************************
   //**************** METHODS *************************************
   //**************************************************************

   //--------------------------------------------------------------
   //----- Global class' methods ----------------------------------
   //--------------------------------------------------------------

   /**
    * Return all faces as an array
   */
   this.facesToArray = function() {
       return [this.greenFace, this.blueFace, this.yellowFace, this.redFace, this.orangeFace, this.whiteFace];
   }

   /**
    * Return the face represented by the letter
    * undefined if wrong letter
   */
   this.getFaceFromLetter = function(letter) {
       switch(letter) {
           case "w":
               return this.whiteFace
           case "b":
               return this.blueFace
           case "o":
               return this.orangeFace
           case "g":
               return this.greenFace
           case "r":
               return this.redFace
           case "y":
               return this.yellowFace
           default: return undefined
       }
   }

   /**
    * Return a string describing the Rubik's Cube.
    * It can be used by our algorithms.
    */
   this.toString = function() {
       res = this.greenFace.toString() + this.orangeFace.toString() + this.whiteFace.toString() + this.blueFace.toString() + this.redFace.toString() + this.yellowFace.toString();

       while(res.indexOf('g') >= 0) {
         res = res.replace('g','U');
       }
       while(res.indexOf('o') >= 0) {
         res = res.replace('o','R');
       }
       while(res.indexOf('w') >= 0) {
         res = res.replace('w','F')
       }
       while(res.indexOf('b') >= 0) {
         res = res.replace('b','D')
       }
       while(res.indexOf('r') >= 0) {
         res = res.replace('r','L')
       }
       while(res.indexOf('y') >= 0) {
         res = res.replace('y','B')
       }
       return res;
   }


   //--------------------------------------------------------------
   //----- Movement related methods -------------------------------
   //--------------------------------------------------------------

   /**
    * Update this.movements with the string given by resolution algorithms.
    */
   this.extractMovements = function(resolutionString) {
       this.movements = resolutionString.replace('\n', '').split(' ');
   }

   // Stores all the movement allowed for the cube.
   // Some of them may seem weird (F,B and U,D) because the 3D cube is
   // actually rotated and the F face is not the same as our.
   let noMovement = this.dimension+0.00000000001; // absurd value
   this.getAllowedMovement = function(move) {
       switch(move){
           case "F" :
           return new Move("F", noMovement, this.dimension-1, noMovement, 1, 1, this.cubicSize);
               break;
           case "F'" :
               return new Move("F", noMovement, this.dimension-1, noMovement, -1, 1, this.cubicSize);
               break;
           case "F2" :
               return new Move("F", noMovement, this.dimension-1, noMovement, 1, 2, this.cubicSize);
               break;
           case "B" :
               return new Move("B", noMovement, 0, noMovement, -1, 1, this.cubicSize);
               break;
           case "B'" :
               return new Move("B", noMovement, 0, noMovement, 1, 1, this.cubicSize);
               break;
           case "B2" :
               return new Move("B", noMovement, 0, noMovement, -1, 2, this.cubicSize);
               break;
           case "L" :
               return new Move("L", 0, noMovement, noMovement, -1, 1, this.cubicSize);
               break;
           case "L'" :
               return new Move("L", 0, noMovement, noMovement, 1, 1, this.cubicSize);
               break;
           case "L2" :
               return new Move("L", 0, noMovement, noMovement, -1, 2, this.cubicSize);
               break;
           case "R" :
               return new Move("R", this.dimension-1, noMovement, noMovement, 1, 1, this.cubicSize);
               break;
           case "R'" :
               return new Move("R", this.dimension-1, noMovement, noMovement, -1, 1, this.cubicSize);
               break;
           case "R2" :
               return new Move("R", this.dimension-1, noMovement, noMovement, 1, 2, this.cubicSize);
               break;
           case "U" :
               return new Move("U", noMovement, noMovement, this.dimension-1, 1, 1, this.cubicSize);
               break;
           case "U'" :
               return new Move("U", noMovement, noMovement, this.dimension-1, -1, 1, this.cubicSize);
               break;
           case "U2" :
               return new Move("U", noMovement, noMovement, this.dimension-1, 1, 2, this.cubicSize);
               break;
           case "D" :
               return new Move("D", noMovement, noMovement, 0, -1, 1, this.cubicSize);
               break;
           case "D'" :
               return new Move("D", noMovement, noMovement, 0, 1, 1, this.cubicSize);
               break;
           case "D2" :
               return new Move("D", noMovement, noMovement, 0, -1, 2, this.cubicSize);
               break;
       }
   }

   /**
    * Returns the current movement among the resolution's movement.
    * Finds the movement in cube.movements thanks to the given index.
    */
   this.getMove = function(index){
       let move;
       let moveString = this.movements[index];
       // if NxN movement, need to specify nb rows.
       // Two types 3Dw (=3D) or Dw (=2D)
       if(moveString.includes("w")){
           if(!isNaN(moveString.charAt(0))){ // 3Dw
               console.log(moveString.substr(1));
               move = this.getAllowedMovement(moveString.charAt(1));
               move.updateMovementWithNbRows(moveString.charAt(0));
           } else{ // Dw
               move = this.getAllowedMovement(moveString.charAt(0));
               move.updateMovementWithNbRows(2);
           }
       } else {
           // casual movement
           move = this.getAllowedMovement(moveString);
       }
       return move;
   }


   //--------------------------------------------------------------
   //----- Animation related methods ------------------------------
   //--------------------------------------------------------------

   /**
    * Create the 3D representation of our Cube.
    * Mix it properly so that our models fits the user's one : we apply the
    * resolution's movement in the opposite.
    */
    this.make3DCube = function(){
      // Create the 3D Cube
       for(let x = 0 ; x < this.dimension ; x++){
           for(let y = 0 ; y < this.dimension ; y++){
               for(let z = 0; z < this.dimension ; z++){
                   let matrix = new p5.Matrix();
                   push();
                   matrix.translate((this.cubicSize*x-MAX_COORDONNEE_CUBE), (this.cubicSize*y-MAX_COORDONNEE_CUBE), (this.cubicSize*z-MAX_COORDONNEE_CUBE));
                   this.cubics.push(new Cubic(matrix,(this.cubicSize*x-MAX_COORDONNEE_CUBE), (this.cubicSize*y-MAX_COORDONNEE_CUBE), (this.cubicSize*z-MAX_COORDONNEE_CUBE)));
                   pop();
               }
           }
       }
       // Mix it
       let move;
       for(let moveIndex = this.movements.length-1 ; moveIndex >= 0 ; moveIndex--){
           move = this.getMove(moveIndex);
           this.applyMovementToCubeCoordinates(move, -move.dir);
       }
    }

   /**
    * Display the 3D representation of the cube.
    */
    this.displayCube = function(){
        for (let i = 0; i < this.cubics.length; i++) {
            push();
            this.cubics[i].display();
            pop();
        }
    }

    /**
     * Function that will call the correct update for the cube coordinates
     * depending on the movement played.
     */
    this.applyMovementToCubeCoordinates = function(move, angleDirection){
        switch(move.name){
            case "F":
                this.applyFToCube(move, angleDirection);
                break;
            case "R":
                this.applyRToCube(move, angleDirection);
                break;
            case "U":
                this.applyUToCube(move, angleDirection);
                break;
            case "B":
                this.applyBToCube(move, angleDirection);
                break;
            case "L":
                this.applyLToCube(move, angleDirection);
                break;
            case "D":
                this.applyDToCube(move, angleDirection);
                break;
            default: // This shall not happen
                console.log("Mouvement non reconnu");
        }
    }

    //-------------------------------------------------------------
    //----- Y axe rotation ----------------------------------------

    /**
     * Update cubics' coordinates concerned by the F move.
     */
    this.applyFToCube = function(move, angleDirection){
        let cubic, x, y, z, updatedCoordinates;
        let angle = angleDirection * HALF_PI;
        for (let i = 0; i < this.cubics.length; i++) {
            cubic = this.cubics[i];
            // take in account F2 style move
            for(let time = 0 ; time < move.nbTime ; time++){
                push();
                // take in account 3F style move
                if (move.y < 75 && cubic.y >= (move.y+(this.cubicSize/2)-0.05)){
                    updatedCoordinates = rotateYAxe(cubic.x, cubic.y, cubic.z, angle);
                    cubic.x = updatedCoordinates[0];
                    cubic.y = updatedCoordinates[1];
                    cubic.z = updatedCoordinates[2];
                    // move cubics' faces
                    cubic.turnFaces('Y', angle);
                }
                pop();
            }
        }
    }
    /**
     * Update cubics' coordinates concerned by the B move.
     */
    this.applyBToCube = function(move, angleDirection){
        let cubic, x, y, z, updatedCoordinates;
        let angle = angleDirection * HALF_PI;
        for (let i = 0; i < this.cubics.length; i++) {
            cubic = this.cubics[i];
            // take in account B2 style move
            for(let time = 0 ; time < move.nbTime ; time++){
                push();
                // take in account 3B style move
                if (move.y < 75 && (cubic.y) <= (move.y+(this.cubicSize/2)+0.05)){
                    updatedCoordinates = rotateYAxe(cubic.x, cubic.y, cubic.z, angle);
                    cubic.x = updatedCoordinates[0];
                    cubic.y = updatedCoordinates[1];
                    cubic.z = updatedCoordinates[2];
                    // move cubics' faces
                    cubic.turnFaces('Y', angle);
                }
                pop();
            }
        }
    }


    //-------------------------------------------------------------
    //----- X axe rotation ----------------------------------------


    /**
     * Update cubics' coordinates concerned by the L move.
     */
    this.applyLToCube = function(move, angleDirection){
        let cubic, x, y, z, updatedCoordinates;
        let angle = angleDirection * HALF_PI;
        for (let i = 0; i < this.cubics.length; i++) {
            cubic = this.cubics[i];
            // take in account L2 style move
            for(let time = 0 ; time < move.nbTime ; time++){
                push();
                // take in account 3L style move
                if (move.x < 75 && cubic.x <= (move.x+(this.cubicSize/2)+0.05)){
                    updatedCoordinates = rotateXAxe(cubic.x, cubic.y, cubic.z, angle);
                    cubic.x = updatedCoordinates[0];
                    cubic.y = updatedCoordinates[1];
                    cubic.z = updatedCoordinates[2];
                    // move cubics' faces
                    cubic.turnFaces('X', angle);
                }
                pop();
            }
        }
    }
    /**
     * Update cubics' coordinates concerned by the R move.
     */
    this.applyRToCube = function(move, angleDirection){
        let cubic, x, y, z, updatedCoordinates;
        let angle = angleDirection * HALF_PI;
        for (let i = 0; i < this.cubics.length; i++) {
            cubic = this.cubics[i];
            // take in account R2 style move
            for(let time = 0 ; time < move.nbTime ; time++){
                push();
                // take in account 3R style move
                if (move.x < 75 && cubic.x >= (move.x+(this.cubicSize/2)-0.05)){
                    updatedCoordinates = rotateXAxe(cubic.x, cubic.y, cubic.z, angle);
                    cubic.x = updatedCoordinates[0];
                    cubic.y = updatedCoordinates[1];
                    cubic.z = updatedCoordinates[2];
                    // move cubics' faces
                    cubic.turnFaces('X', angle);
                }
                pop();
            }
        }
    }


    //-------------------------------------------------------------
    //----- Z axe rotation ----------------------------------------

    /**
     * Update cubics' coordinates concerned by the U move.
     */
    this.applyUToCube = function(move, angleDirection){
        let cubic, x, y, z, updatedCoordinates;
        let angle = angleDirection * HALF_PI;
        for (let i = 0; i < this.cubics.length; i++) {
            cubic = this.cubics[i];
            // take in account U2 style move
            for(let time = 0 ; time < move.nbTime ; time++){
                push();
                // take in account 3U style move
                if (move.z < 75 && cubic.z >= (move.z+(this.cubicSize/2)-0.05)){
                    updatedCoordinates = rotateZAxe(cubic.x, cubic.y, cubic.z, angle);
                    cubic.x = updatedCoordinates[0];
                    cubic.y = updatedCoordinates[1];
                    cubic.z = updatedCoordinates[2];
                    // move cubics' faces
                    cubic.turnFaces('Z', angle);
                }
                pop();
            }
        }
    }
    /**
     * Update cubics' coordinates concerned by the D move.
     */
    this.applyDToCube = function(move, angleDirection){
        let cubic, x, y, z, updatedCoordinates;
        let angle = angleDirection * HALF_PI;
        for (let i = 0; i < this.cubics.length; i++) {
            cubic = this.cubics[i];
            // take in account D2 style move
            for(let time = 0 ; time < move.nbTime ; time++){
                push();
                // take in account 3D style move
                if (move.z < 75 && cubic.z <= (move.z+(this.cubicSize/2)+0.05)){
                    updatedCoordinates = rotateZAxe(cubic.x, cubic.y, cubic.z, angle);
                    cubic.x = updatedCoordinates[0];
                    cubic.y = updatedCoordinates[1];
                    cubic.z = updatedCoordinates[2];
                    // move cubics' faces
                    cubic.turnFaces('Z', angle);
                }
                pop();
            }
        }
    }
}
