function FaceSquare() {

    this.meanR = 0;
    this.meanG = 0;
    this.meanB = 0;

    this.color = "";

    this.similarity = function(reference_color) {
        return ( 1 / (1 + sqrt(
            pow(reference_color.r - this.meanR, 2) +
            pow(reference_color.g - this.meanG, 2) +
            pow(reference_color.b - this.meanB, 2)
        )));
    }

    this.toString = function() {
        return this.color;
    }

}
