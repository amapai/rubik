/**
 * Calculates new coordinates for an rotation around the X axe.
 * x,y,z are coordinates, angle the angle of rotation.
 */
function rotateXAxe(x, y, z, angle){
    let coordinates = [];
    coordinates[0] = x;
    coordinates[1] = (y * Math.cos(angle)) - (z * Math.sin(angle));
    coordinates[2] = (y * Math.sin(angle)) + (z * Math.cos(angle));
    return coordinates;
}
/**
 * Calculates new coordinates for an rotation around the Y axe.
 * x,y,z are coordinates, angle the angle of rotation.
 */
function rotateYAxe(x, y, z, angle){
    let coordinates = [];
    coordinates[0] = (z * Math.sin(angle)) + (x * Math.cos(angle));
    coordinates[1] = y;
    coordinates[2] = (z * Math.cos(angle)) - (x * Math.sin(angle));
    return coordinates;
}
/**
 * Calculates new coordinates for an rotation around the Z axe.
 * x,y,z are coordinates, angle the angle of rotation.
 */
function rotateZAxe(x, y, z, angle){
    let coordinates = [];
    coordinates[0] = (x * Math.cos(angle)) - (y * Math.sin(angle));
    coordinates[1] = (x * Math.sin(angle)) + (y * Math.cos(angle));
    coordinates[2] = z;
    return coordinates;
}
