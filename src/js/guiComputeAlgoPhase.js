var selectedColor;
var nbAlgoStillComputing;

/**
 * Calls gui's creation and starts algo's computing depending on the
 * dimension of the cube.
 */
function resolve(){
    makeComputeAlgoGui();
    if(cube.dimension == 3){
        nbAlgoStillComputing = 3;
        contacter3x3("kociemba", cube.toString(), loadKociemba);
        contacter3x3("korf", cube.toString(), loadKorf);
        contacter3x3("cpc",cube.toString(), loadCoucheParCouche);
    } else {
        nbAlgoStillComputing = 1;
        contacterNxN(cube.toString(), loadNxN);
    }
}

/**
 * Create the gui.
 */
function makeComputeAlgoGui(){
    let body = document.getElementById("content");
    if(cube.dimension == 3){
        body.innerHTML = `
        <div id="loading" class="col-md-4"><div class="cube">
            <div class="side"></div>
            <div class="side"></div>
            <div class="side"></div>
            <div class="side"></div>
            <div class="side"></div>
            <div class="side"></div>
        </div>
        </div>
        <div id="list-algorithms" class="col-md-6">
            <div class="row"><h2>Recherche de solutions</h2></div>
            <div class="row"><h3>Veuillez sélectionner la méthode de résolution de votre choix. Attention, certaines peuvent être encore en train d'être calculées !</h3></div>
            <div id="btnCpc" class="row">
                <button type="button" class="disabled btn btn-primary btn-lg btn-block">Méthode couches par couches</button><br/>
            </div>
            <div id="btnKociemba" class="row">
                <button type="button" class="disabled btn btn-success btn-lg btn-block">Méthode Kociemba</button><br/>
            </div>
            <div id="btnKorf" class="row">
                <button type="button" class="disabled btn btn-danger btn-lg btn-block">Méthode Korf</button><br/>
            </div>
        </div>`;
    } else {
        body.innerHTML = `
        <div id="loading" class="col-md-4"><div class="cube">
            <div class="side"></div>
            <div class="side"></div>
            <div class="side"></div>
            <div class="side"></div>
            <div class="side"></div>
            <div class="side"></div>
          </div>
        </div>
        <div id="list-algorithms" class="col-md-6">
            <div class="row"><h2>Recherche de solutions</h2></div>
            <div class="row"><h3>Veuillez sélectionner la méthode de résolution de votre choix. Attention, certaines peuvent être encore en train d'être calculée !</h3></div>
            <div id="btnNxN" class="row">
                <button type="button" class="disabled btn btn-danger btn-lg btn-block">Méthode N x N</button><br/>
            </div>
        </div>`;
    }

}


/**
 * Receives the resolution string for CPC's method.
 * Activate the button and display the number of movements.
 * Check if all the algo terminate and stop gif if so.
 */
function loadCoucheParCouche(resolutionString){
    cube.extractMovements(resolutionString);
    let btn = document.getElementById("btnCpc");
    btn.innerHTML = `<button type="button" onclick="makeAnimationGUI('couche par couche', \``+resolutionString+`\`)" class="btn btn-info btn-lg btn-block">Méthode couche par couche (`+ cube.movements.length +` mouvements)</button>`;
    nbAlgoStillComputing--;
    if(nbAlgoStillComputing==0){
        stopLoadingLoop();
    }
}

/**
 * Receives the resolution string for Kociemba's method.
 * Activate the button and display the number of movements.
 * Check if all the algo terminate and stop gif if so.
 */
function loadKociemba(resolutionString){
    cube.extractMovements(resolutionString);
    let btn = document.getElementById("btnKociemba");
    btn.innerHTML = `<button type="button" onclick="makeAnimationGUI('Kociemba', \``+resolutionString+`\`)" class="btn btn-success btn-lg btn-block">Méthode Kociemba (`+ cube.movements.length +` mouvements)</button>`;
    nbAlgoStillComputing--;
    if(nbAlgoStillComputing==0){
        stopLoadingLoop();
    }
}

/**
 * Receives the resolution string for Korf's method.
 * Activate the button and display the number of movements.
 * Check if all the algo terminate and stop gif if so.
 */
function loadKorf(resolutionString){
    cube.extractMovements(resolutionString);
    let btn = document.getElementById("btnKorf");
    btn.innerHTML = `<button type="button" onclick="makeAnimationGUI('Korf', \``+resolutionString+`\`)" class="btn btn-danger btn-lg btn-block">Méthode Korf (`+ cube.movements.length +` mouvements)</button>`;
    nbAlgoStillComputing--;
    if(nbAlgoStillComputing==0){
        stopLoadingLoop();
    }
}

/**
 * Receives the resolution string for NxN's method.
 * Activate the button and display the number of movements.
 * Check if all the algo terminate and stop gif if so.
 */
function loadNxN(resolutionString){
    cube.extractMovements(resolutionString);
    let btn = document.getElementById("btnNxN");
    btn.innerHTML = `<button type="button" onclick="makeAnimationGUI('N x N', \``+resolutionString+`\`)" class="btn btn-danger btn-lg btn-block">Méthode N x N (`+ cube.movements.length +` mouvements)</button>`;
    stopLoadingLoop();
}

/**
 * Replaces the cube loop by a picture to show user calculations are over.
 */
function stopLoadingLoop(){
    let loading = document.getElementById("loading");
    loading.innerHTML = `<img src="asset/cube-logo.png" style="height:350px; margin-left:-50px;">`;
}
