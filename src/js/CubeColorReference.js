function MeanColor(_r, _g, _b) {
    this.r = _r;
    this.g = _g;
    this.b = _b;
}

function CubeColorReference() {

    this.meanGreen = undefined;
    this.meanBlue = undefined;
    this.meanYellow = undefined;
    this.meanRed = undefined;
    this.meanOrange = undefined;
    this.meanWhite = undefined;
}
