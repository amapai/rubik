var activeMovement = 0;
var speedBetweenMovements = 3000 ;

var selectedColor;
var isAnimationTime = false;


/******************************************************************************/
/******************************************************************************/
/***************************** ANIMATION **************************************/
/******************************************************************************/
/******************************************************************************/


/**
 * Builds interface's skeleton then call interfaces' methods.
 */
function makeAnimationGUI(methodName, resolutionString) {
    cube.extractMovements(resolutionString);
    cube.make3DCube();
    currentMove = cube.getMove(activeMovement);
    isAnimationTime = true;
    let pContent = document.getElementById("content");
    pContent.innerHTML = `<div id="content"  class="row">
        <div id="panel-toolbox" class="col-md-3"></div>
        <div id="panel-center" class="col-md-6">
            <div class="row">
                <div id="panel-instructions" class="col-md-11"></div>
            </div>
            <div class="row">
                <div id="camera" class="col-md-11"></div>
            </div>
            <div class="row">
                <div id="panel-button" class="col-md-11"></div>
            </div>
        </div><!--central column-->
        <div id="panel-list" class="col-md-3"></div>
    </div>`;
    panelInstructionsAnimation(methodName);
    panelToolboxAnimation();
    panelListAnimation();
    panelCameraAnimation();
    panelButtonAnimation();
}

/**
 * Calls wanted functions to update our GUI when playing the resolution.
 */
function updateAnimationGui(){
    applyCubeTransformation();
    panelToolboxAnimation();
    panelListAnimation();
    panelCameraAnimation();
    panelButtonAnimation();
}

/**
 * fait en sorte que le cube soit celui MAJ avec le précédent mouvement.
 */
function applyCubeTransformation(){

}

/**
 * Displays general instructions.
 */
function panelInstructionsAnimation(methodName){
    let pInstructions = document.getElementById("panel-instructions");
    pInstructions.innerHTML = `<h3> Méthode ` + methodName + `    <small> Suivez les mouvements pour résoudre le cube.</small></h3>`;
}


/**
 * Puts vocal commands' instructions at the left of the interface.
 */
function panelToolboxAnimation() {
    let pToolbox = document.getElementById("panel-toolbox");
    pToolbox.innerHTML = `<h3>Commandes Vocales</h3></br>
        <div class="row">
            <p>&#171; Stop &#187; : arrête l'animation</p>
        </div></br>
        <div class="row">
            <p>&#171; Continue &#187; : reprend l'animation mise en pause</p>
        </div></br>
        <div class="row">
            <p>&#171; Suivant &#187; : passe au mouvement suivant</p>
        </div></br>
        <div class="row">
            <p>&#171; Retour &#187; : revient au mouvement précédant</p>
        </div></br>`;
}

/**
 * Displays the lists of movements at the right of the interface.
 * Buttons are not clickable. This method use makeMovementButtons() to
 * display the image associated with the movement.
 */
function panelListAnimation() {
    let pList = document.getElementById("panel-list");
    pList.innerHTML = `<h3>Mouvements</h3>
    <h4>`+ (activeMovement+1) +` sur `+ cube.movements.length + `</h4>
    <div>`;
    let i;
    if (cube.movements.length <= 7){
        for (i = 0 ; i < cube.movements.length ; i++) {
            if( i == activeMovement) {
                pList.innerHTML += makeMovementButtons("", i);
            } else {
                pList.innerHTML += makeMovementButtons("disabled", i);
            }
        }
    } else if(activeMovement < 2){
        for (i = 0 ; i < 5 ; i++) {
            if( i == activeMovement) {
                pList.innerHTML += makeMovementButtons("", i);
            } else {
                pList.innerHTML += makeMovementButtons("disabled", i);
            }
        }
        pList.innerHTML += `<button class="btn btn-lg btn-block disabled dark-text">...</button>`;
    } else if (activeMovement >= cube.movements.length-3 ) {
        pList.innerHTML += `<button class="btn btn-lg btn-block disabled dark-text">...</button>`;
        for (i = cube.movements.length-5 ; i < cube.movements.length ; i++) {
            if( i == activeMovement) {
                pList.innerHTML += makeMovementButtons("", i);
            } else {
                pList.innerHTML += makeMovementButtons("disabled", i);
            }
        }
    } else {
        pList.innerHTML += `<button class="btn btn-lg btn-block disabled dark-text">...</button>`;
        for (i = activeMovement-1 ; i < activeMovement+3 ; i++) {
            if( i == activeMovement) {
                pList.innerHTML += makeMovementButtons("", i);
            } else {
                pList.innerHTML += makeMovementButtons("disabled", i);
            }
        }
        pList.innerHTML += `<button class="btn btn-lg btn-block disabled dark-text">...</button>`;
    }
    pList.innerHTML += `</div>`;
}

/**
 * Used by panelListAnimation().
 * Create a button with correct movement and picture associated
 */
function makeMovementButtons(disable, i) {
    return "<button class=\"btn btn-lg btn-block dark-text "+disable+"\">"+ displayPictureMovement(cube.movements[i], "width:50px; height:50px;") + cube.movements[i] +"</button>";
}

/**
 * Display the movement to do thanks to a 3D Cube.
 */
function panelCameraAnimation(){
    let pCamera = document.getElementById("camera");
    pCamera.innerHTML = `
    <div id="animation" style="height:300px;width:500px;"></div>
    <div id="instructionAnimation">
        `+ displayMovementInstruction() +`
    </div>`;
    createCanvas(550,300,WEBGL).parent('animation');
    background(250);
    // If we have a NxN movement, format differs slightly.
    currentMove.start();
}

/**
 * Change only instruction and animation of the movement.
 */
function animateCube(){
    let pCamera = document.getElementById("instructionAnimation");
    pCamera.innerHTML = displayMovementInstruction();
    currentMove = cube.allowedMovements[cube.movements[activeMovement]];
    currentMove.start();
}

/**
 * Displays picture associated to movement.
 */
function displayPictureMovement(movement, style){
    switch(currentMove.name){
        case "F":
            return `<img src="asset/cube-color-F.png" style="`+ style +`">`;
        case "F'":
            return `<img src="asset/cube-color-FA.png" style="`+ style +`">`;
        case "F2":
            return `<img src="asset/cube-color-F.png" style="`+ style +`">`;
        case "R":
            return `<img src="asset/cube-color-R.png" style="`+ style +`">`;
        case "R'":
            return `<img src="asset/cube-color-RA.png" style="`+ style +`">`;
        case "R2":
            return `<img src="asset/cube-color-R.png" style="`+ style +`">`;
        case "U":
            return `<img src="asset/cube-color-U.png" style="`+ style +`">`;
        case "U'":
            return `<img src="asset/cube-color-UA.png" style="`+ style +`">`;
        case "U2":
            return `<img src="asset/cube-color-U.png" style="`+ style +`">`;
        case "B":
            return `<img src="asset/cube-color-B.png" style="`+ style +`">`;
        case "B'":
            return `<img src="asset/cube-color-BA.png" style="`+ style +`">`;
        case "B2":
            return `<img src="asset/cube-color-B.png" style="`+ style +`">`;
        case "L":
            return `<img src="asset/cube-color-L.png" style="`+ style +`">`;
        case "L'":
            return `<img src="asset/cube-color-LA.png" style="`+ style +`">`;
        case "L2":
            return `<img src="asset/cube-color-L.png" style="`+ style +`">`;
        case "D":
            return `<img src="asset/cube-color-D.png" style="`+ style +`">`;
        case "D'":
            return `<img src="asset/cube-color-DA.png" style="`+ style +`">`;
        case "D2":
            return `<img src="asset/cube-color-D.png" style=`+ style +`">`;
        default:
            return `<img src="asset/cube-color.png" style="`+ style +`">`;
    }
}

/**
 * Displays correct instruction depending the movements.
 */
function displayMovementInstruction(){
    switch(cube.movements[activeMovement]){
        case "F":
            return "<h3>F<small> Faites pivoter la face avant dans le sens horaire.</small></h3>";
        case "F'":
            return "<h3>F'<small> Faites pivoter la face avant dans le sens anti-horaire.</small></h3>";
        case "F2":
            return "<h3>F2<small> Faites pivoter la face avant deux fois dans le sens horaire.</small></h3>";
        case "R":
            return "<h3>R<small> Faites pivoter la face droite dans le sens horaire.</small></h3>";
        case "R'":
            return "<h3>R'<small> Faites pivoter la face droite dans le sens anti-horaire.</small></h3>";
        case "R2":
            return "<h3>R2<small> Faites pivoter la face droite deux fois dans le sens horaire.</small></h3>";
        case "U":
            return "<h3>U<small> Faites pivoter la première couronne dans le sens horaire.</small></h3>";
        case "U'":
            return "<h3>U'<small> Faites pivoter la première couronne dans le sens anti-horaire.</small></h3>";
        case "U2":
            return "<h3>U2<small> Faites pivoter la première couronne deux fois dans le sens horaire.</small></h3>";
        case "B":
            return "<h3>B<small> Faites pivoter la face arrière dans le sens horaire.</small></h3>";
        case "B'":
            return "<h3>B'<small> Faites pivoter la face arrière dans le sens anti-horaire.</small></h3>";
        case "B2":
            return "<h3>B2<small> Faites pivoter la face arrière deux fois dans le sens horaire.</small></h3>";
        case "L":
            return "<h3>L<small> Faites pivoter la face gauche dans le sens horaire.</small></h3>";
        case "L'":
            return "<h3>L'<small> Faites pivoter la face gauche dans le sens anti-horaire.</small></h3>";
        case "L2":
            return "<h3>L2<small> Faites pivoter la face gauche deux fois dans le sens horaire.</small></h3>";
        case "D":
            return "<h3>D<small> Faites pivoter la dernière couronne dans le sens horaire.</small></h3>";
        case "D'":
            return "<h3>D'<small> Faites pivoter la dernière couronne dans le sens anti-horaire.</small></h3>";
        case "D2":
            return "<h3>D2<small> Faites pivoter la dernière couronne deux fois dans le sens horaire.</small></h3>";
        default:
            return "<h3>"+currentMove.name+"<small> Faites pivoter la ou les faces "+currentMove.name+" dans le sens indiquépar l'animation.</small></h3>";
    }
}

/**
 * Displays buttons. Disable if necessary (1st or last movement).
 * Calls btnPreviousMovement() btnPlay() btnPause() and btnNextMovement().
 */
function panelButtonAnimation(){
    let pButtons = document.getElementById("panel-button");
    pButtons.innerHTML = "</br>";
    if(activeMovement == 0){
        pButtons.innerHTML +=`<button class="dark-text btn btn-lg disabled"><i class="material-icons">skip_previous </i></button>`;
    } else {
        pButtons.innerHTML +=`<button id="btn-previous" onclick="btnPreviousMovement()" class="dark-text btn btn-lg"><i class="material-icons">skip_previous </i></button>`;
    }
    pButtons.innerHTML +=`
    <button id="btn-play" onclick="play = setInterval(btnNextMovement, `+speedBetweenMovements+`)" class="dark-text btn btn-lg"><i class="material-icons"> play_arrow </i></button>
    <button id="btn-pause" onclick="clearTimeout(play)" class="dark-text btn btn-lg"><i class="material-icons"> pause </i></button>`;
    if(activeMovement == (cube.movements.length - 1)){
        pButtons.innerHTML +=`<button class="dark-text btn btn-lg disabled"><i class="material-icons"> skip_next</i></button>`;
    } else {
        pButtons.innerHTML +=`<button id="btn-next" onclick="btnNextMovement()" class="dark-text btn btn-lg"><i class="material-icons"> skip_next</i></button>`;
    }
}

/**
 * Displays previous movement.
 * Change the cubics coordinates depending on the movement : see file
 * updateCubeCoordinates.js
 * Here the currentMove is not applied. To go at previous movement,
 * we need to undo the previous cube movement.That is why we
 * reverse the angle.
 */
function btnPreviousMovement() {
    if(activeMovement >= 0){
        activeMovement--;
        currentMove = cube.getMove(activeMovement);
        cube.applyMovementToCubeCoordinates(currentMove, -currentMove.dir);
        updateAnimationGui();
    }
}
/**
 * Displays next movement.
 * Change the cubics coordinates depending on the movement : see file
 * updateCubeCoordinates.js
 */
function btnNextMovement() {
    if(activeMovement < cube.movements.length-1){
        cube.applyMovementToCubeCoordinates(currentMove, currentMove.dir);
        activeMovement++;
        currentMove = cube.getMove(activeMovement);
        updateAnimationGui();
    }
}
