function Face(n, color) {
    this.color = color;

    //this.positionVector = vpos;
    this.isInitialized = false;

    // initialisation des carrés de la face
    this.squareInitialisation = function(n, color){
        let s = [];
        for(let i = 0; i < n; i++) {
            s[i] = [];
            for(var j = 0; j < n; j++) {
                s[i][j] = new FaceSquare();
                if( n % 2 == 1
                    && i == floor(n/2)
                    && j == floor(n/2)){
                        s[i][j].color = color;
                }
            }
        }
        return s;
    }

    this.squares = this.squareInitialisation(n, color);

    /**
     * Returns the color code of a cubic
     * Useful for a nice display
     */
    this.squareGetColor = function(colorCode){
        switch (colorCode) {
            case "w":
                return "#FFFFFF";
            case "r":
                return "#EB5757";
            case "b":
                return "#2D9CDB";
            case "o":
                return "#F2994A";
            case "g":
                return "#219653";
            case "y":
                return "#F2C94C";
            default:
                return "grey";
        }
    }

    /**
     * Returns an size-4-array in which colors of borders' faces are stored.
     * Up face = 0 ; right face = 1 ; bottom = 2 and left = 3.
     * This function is used in toSvg()
     */
     this.getBorders = function(){
         let borders = [];
         switch (this.color) {
             case "w":
                borders[0] = "#219653" ; /* G */
                borders[1] = "#F2994A" ; /* O */
                borders[2] = "#2D9CDB" ; /* B */
                borders[3] = "#EB5757" ; /* R */
                 break;
             case "r":
                 borders[0] = "#219653" ; /* G */
                 borders[1] = "#FFFFFF" ; /* W */
                 borders[2] = "#2D9CDB" ; /* B */
                 borders[3] = "#F2C94C" ; /* Y */
                 break;
             case "b":
                 borders[0] = "#FFFFFF" ; /* W */
                 borders[1] = "#F2994A" ; /* O */
                 borders[2] = "#F2C94C" ; /* Y */
                 borders[3] = "#EB5757" ; /* R */
                 break;
             case "o":
                 borders[0] = "#219653" ; /* G */
                 borders[1] = "#F2C94C" ; /* Y */
                 borders[2] = "#2D9CDB" ; /* B */
                 borders[3] = "#FFFFFF" ; /* W */
                 break;
             case "g":
                 borders[0] = "#F2C94C" ; /* Y */
                 borders[1] = "#F2994A" ; /* O */
                 borders[2] = "#FFFFFF" ; /* W */
                 borders[3] = "#EB5757" ; /* R */
                 break;
             case "y":
                 borders[0] = "#219653" ; /* G */
                 borders[1] = "#EB5757" ; /* R */
                 borders[2] = "#2D9CDB" ; /* B */
                 borders[3] = "#F2994A" ; /* O */
                 break;
            default:
                 borders[0] = "#219653" ; /* G */
                 borders[1] = "#EB5757" ; /* R */
                 borders[2] = "#2D9CDB" ; /* B */
                 borders[3] = "#F2994A" ; /* O */
                 break;
         }
         return borders;
     }

    /**
     * Returns a string describing the squares composing the face.
     * Replace the "," by nothing.
     */
     this.toString = function() {
         return this.squares.toString().replace(/,/gi, '');
     }

    /**
     * Returns the HTML code for a SVG depicting the face.
     */
    this.toSvg = function() {
        let borders = this.getBorders();

        let svgString = `<svg width="40" height="40">
            <!-- Lines : help to locate face in the cube -->
            <line x1="0" y1="0" x2="40" y2="0" style="stroke:` + borders[0] + `;stroke-width:10" />
            <line x1="40" y1="0" x2="40" y2="40" style="stroke:` + borders[1] + `;stroke-width:10" />
            <line x1="0" y1="40" x2="40" y2="40" style="stroke:` + borders[2] + `;stroke-width:10" />
            <line x1="0" y1="0" x2="0" y2="40" style="stroke:` + borders[3] + `;stroke-width:10" />`;

        let x = 5;
        let y = 5;
        let i, j;
        for(i = 0 ; i < n ; i++){
            for(j = 0 ; j < n ; j++){
                svgString += `<rect x="`+ x +`" y="`+ y +`" width="`+(30/n)+`" height="`+(30/n)+`" style="fill:`+ this.squareGetColor(this.squares[i][j].toString()) +`;stroke:black;stroke-width:1;"/>`;
                    x+=(30/n); // count twice 5 px for borders (40 - 2*10 = 30).
            }
            x = 5;
            y +=(30/n);
        }
        svgString += `</svg>`;
        return svgString;
    }
}
