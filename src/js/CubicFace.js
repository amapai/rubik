/**
 * Represents faces of a single cubic.
 * Used to display a 3D cube.
 */
function CubicFace(normal, color) {
    this.normal = normal; // position vector
    this.color = color; // color of the face


    /**
     * Display a single face of a cubic (in a cube).
     */
    this.display = function() {
      push();
      fill(this.color);
      noStroke();
      rectMode(CENTER);
      translate(0.5*this.normal[0], 0.5*this.normal[1], 0.5*this.normal[2]);
      if (abs(this.normal[0]) > 0) {
          rotateY(HALF_PI);
      } else if (abs(this.normal[1]) > 0) {
          rotateX(HALF_PI);
      }
      box(cube.cubicSize-1, cube.cubicSize-1, cube.cubicSize-1);
      pop();
    }

    /**
     * TODO comm
     */
     this.turnFace = function(axe, angleDirection){
         let updatedCoordinates;
         push();
         switch(axe){
             case 'X':
                updatedCoordinates = rotateXAxe(this.normal[0], this.normal[1], this.normal[2], angleDirection);
                break;
             case 'Y':
                updatedCoordinates = rotateYAxe(this.normal[0], this.normal[1], this.normal[2], angleDirection);
                break;
             case 'Z':
                updatedCoordinates = rotateZAxe(this.normal[0], this.normal[1], this.normal[2], angleDirection);
                break;
            default:
                break;
         }
         this.normal[0] = updatedCoordinates[0];
         this.normal[1] = updatedCoordinates[1];
         this.normal[2] = updatedCoordinates[2];
         pop();
     }
}
