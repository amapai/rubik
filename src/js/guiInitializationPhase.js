// *****************************************************************************
// Initialization happens as a state machine : functions colorFaceFocused() are
// enter points that updates interfaces.
// *****************************************************************************

var activeFace = "g";
var selectedColor;

/**
 * Initialize the cube, activate our display and begin the cube initialisation.
 */
function beginInitialization() {
    activeFace = "g";
    // Initialize the Cube
    let n = document.getElementById("cubeDimension");
    if (n.value === "" || n.value <=0){
        n.value = 3;
    }
    cubeDimension = parseInt(n.value);
    // creation of the cube object
    cube = new Cube(cubeDimension);

    // initialize space between lines
    space = height / (cubeDimension + 2);
    lineSize = 2;
    detectionOffset = floor(30 / cubeDimension);
    detectionSquareSize = space - 2 * detectionOffset;

    // Hide the homepage and display the main part
    let pAccueil = document.getElementById("accueil");
    pAccueil.style.display = "none";
    let pContent = document.getElementById("content");
    pContent.style.display = "inline";

    panelInfoInitialization();
    greenFaceFocused();
}

// *****************************************************************************
// ******************************** TOOLBOX ********************************----
// *****************************************************************************

/* Called only once, display informations */
function panelInfoInitialization() {
    let pToolbox = document.getElementById("panel-toolbox");
    if(cubeDimension % 2 == 0) {
        pToolbox.innerHTML = `
            <h2>Indications :</h2>
            <h3> Commande vocale </h3>
            <p> Pour passer à la face suivante, dites "Suivant". </p>
            <h3> Conseils </h3>
            <p> Vous utilisez un cube pair, une phase d'initialisation des couleurs est nécessaire. </p>
            </br>
            <p> Gardez votre cube d'une façon cohérente dans l'espace, en attribuant une couleur à chaque face. </p>
            <h4> Les couleurs usuelles sont : </h4>
            <ul>
                <li> Vert en haut </li>
                <li> Blanc en face </li>
                <li> Rouge à gauche </li>
                <li> Jaune à l'arrière </li>
                <li> Orange à droite </li>
                <li> Bleu en dessous </li>
            </ul>
        `;
    } else {
        pToolbox.innerHTML = `
            <h2>Indications :</h2>
            <h3> Commande vocale </h3>
            <p> Pour passer à la face suivante, dites "Suivant". </p>
            <h3> Conseils </h3>
            <p> Vous pouvez naviguer entre les différentes faces en utilisant les boutons à droite de l'écran. </p>
            </br>
            <p> Une fois la saisie terminée, il vous sera possible de modifier les couleurs manuellement afin de corriger toute erreur de détection. Il s'agit d'une phase importante, n'oubliez pas de tout vérifier. </p>
        `;
    }
}

/**
 * Called only once, display our palette and allows user to manually changes
 * cubics' color by clicking buttons.
 */
function makeToolboxPanel() {
    let pToolbox = document.getElementById("panel-toolbox");
    pToolbox.innerHTML = `<h2>Palette :</h2></br>
        <div class="row">
            <div class="col-sm-4">
                <button id="" onclick="selectColor('r')" class="btnPaletteRed"></button>
            </div>
            <div class="col-sm-4">
                <button id="" onclick="selectColor('b')" class="btnPaletteBlue"></button>
            </div>
        </div></br>
        <div class="row">
            <div class="col-sm-4">
                <button id="" onclick="selectColor('g')" class="btnPaletteGreen"></button>
            </div>
            <div class="col-sm-4">
                <button id="" onclick="selectColor('o')" class="btnPaletteOrange"></button>
            </div>
        </div></br>
        <div class="row">
            <div class="col-sm-4">
                <button id="" onclick="selectColor('y')" class="btnPaletteYellow"></button>
            </div>
            <div class="col-sm-4">
                <button id="" onclick="selectColor('w')"
                class="btnPaletteWhite"></button>
            </div>
        </div>`;
}

/**
 * Choose a color using the color panel.
 */
function selectColor(color) {
    selectedColor = color;
}

// *****************************************************************************
// ************************** CHANGING INTERFACES ******************************
// *****************************************************************************

// Following functions updates interface depending the face to initialize.

/**
 * List the face to initialize.
 * Params are "" OR "active", define which face is "focused"
 */
function makeFaceListPanel(gIsActive, wIsActive, rIsActive, yIsActive, oIsActive, bIsActive) {
    let pList = document.getElementById("panel-list");
    pList.innerHTML = `<h2>Faces</h2>
    <button onclick="navigateBetweenFaces('')" class="btn-success btn-lg btn-block dark-text `+ gIsActive +` ">`+ cube.greenFace.toSvg() +` Face verte</button>
    <button onclick="navigateBetweenFaces('g')" class="btn-default btn-lg btn-block dark-text `+ wIsActive +` ">`+ cube.whiteFace.toSvg() +` Face blanche</button>
    <button onclick="navigateBetweenFaces('w')" class="btn-danger btn-lg btn-block dark-text `+ rIsActive +` ">`+ cube.redFace.toSvg() +` Face rouge</button>
    <button onclick="navigateBetweenFaces('r')" style="background-color:#F2C94C; border-color:yellow;" class="btn-default btn-lg btn-block dark-text `+ yIsActive +` ">`+ cube.yellowFace.toSvg() +` Face jaune</button>
    <button onclick="navigateBetweenFaces('y')" class="btn-warning btn-lg btn-block dark-text `+ oIsActive +` ">`+ cube.orangeFace.toSvg() +` Face orange</button>
    <button onclick="navigateBetweenFaces('o')" class="btn-info btn-lg btn-block dark-text `+ bIsActive +` ">`+ cube.blueFace.toSvg() +` Face bleue</button>`;
}

/**
 * Display a to text help the user. Param color is the changing word of the
 * sentence : depends on which face the user is working on.
 */
function makeInstructionsPanel(color) {
    let pInstructions = document.getElementById("panel-instructions");
    if(areColorsDefined) {
        pInstructions.innerHTML = "<h3>Présentez la face " + color + " à l'écran en tenant compte de l'orientation demandée. Appuyez ensuite sur le bouton pour passer à la face suivante.</h3>";
    } else {
        pInstructions.innerHTML = "<h3>Présentez un cube de couleur " + color + " à l'écran. Appuyez ensuite sur le bouton pour passer à la couleur suivante.</h3>";
    }
}

/**
 * Display the action button. Texte change weither we have finished initialize
 * the last face or not.
 * If we are at the last face to initialize (blue face), the button will call
 * a modal to resume the faces.
 */
function makeButtonNext() {
    let pButton = document.getElementById("panel-button");
    if(activeFace == "b"){
        if(areColorsDefined) {
            pButton.innerHTML = `<button id="btn-nextFace" onclick="makeResumeModal()" class="btn btn-default dark-text btn-lg btn-block" type="submit" data-toggle="modal" data-target="#resumeModal">Initialiser la face bleue et terminer l'initialisation</button>`;
        } else {
            pButton.innerHTML = `<button id="btn-nextFace" onclick="transition()" class="btn btn-default dark-text btn-lg btn-block" type="submit">Calibrer la face bleue et passer à l'acquisition du cube</button>`;
        }
    } else {
        pButton.innerHTML = `<button id="btn-nextFace" onclick="buttonInitializeFace('`+activeFace+`')" class="btn btn-default btn-lg btn-block dark-text" type="submit">Enregistrer et passer à la face suivante</button>`;
    }
}

/* transition entre calibration et détection */
function transition() {
    initializeFace();
    areColorsDefined = true;
    greenFaceFocused();
}

/**
 * A resume of initialized faces popup when the last face is initialized.
 * Allows user to check everything is fine before calculation.
 */
function makeResumeModal(){
    initializeFace();
    knnColorClassifier();
    makeFaceListPanel();
    makeToolboxPanel();
    let pInstructions = document.getElementById("modal");
    pInstructions.innerHTML = `
    <div id="resumeModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title dark-text">Vérifiez que les faces soient bien initialisées et lancez le calcul de la résolution.</h3>
                </div>
                <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 dark-text">`+ cube.greenFace.toSvg()+` Face verte</div>
                    <div class="col-md-4 dark-text">`+ cube.redFace.toSvg()+` Face rouge</div>
                    <div class="col-md-4 dark-text">`+ cube.whiteFace.toSvg()+` Face blanche</div>
                </div>
                <div class="row">
                    <div class="col-md-4 dark-text">`+ cube.yellowFace.toSvg()+` Face jaune</div>
                    <div class="col-md-4 dark-text">`+ cube.orangeFace.toSvg()+` Face orange</div>
                    <div class="col-md-4 dark-text">`+ cube.blueFace.toSvg()+` Face bleue</div>
                </div>
                </div>
                <div class="modal-footer">
                    <button onclick="resolve()" class="btn-lg btn-block dark-text" data-dismiss="modal">Calculer la résolution</button>
                    <button onclick="greenFaceFocused()" class="btn-lg btn-block dark-text" data-dismiss="modal">Corriger le cube</button>
                </div>
            </div>
        </div>
    </div>`;
}



// *****************************************************************************
// ************************ INITIALIZATION STEPS *******************************
// *****************************************************************************

/**
 * Initialize the active face and display the next.
 */
function buttonInitializeFace(activeFace){
    initializeFace();
    navigateBetweenFaces(activeFace);
}

/**
 * Check what face is currently focused, then display the next.
 * If initialization is over, then send the cube to algorithms and change GUI
 */
function navigateBetweenFaces(activeFace){
    switch(activeFace) {
        case "g":
            whiteFaceFocused()
            break;
        case "w":
            redFaceFocused();
            break;
        case "r":
            yellowFaceFocused()
            break;
        case "y":
            orangeFaceFocused()
            break;
        case "o":
            blueFaceFocused()
            break;
        case "b":
            break;
        default:
            greenFaceFocused();
    }
}

function greenFaceFocused(){
    activeFace = "g";
    makeInstructionsPanel("verte");
    makeButtonNext();
    makeFaceListPanel("active", "", "", "", "", "");
}
function whiteFaceFocused() {
    activeFace = "w";
    makeInstructionsPanel("blanche");
    makeButtonNext();
    makeFaceListPanel("", "active", "", "", "", "");
}
function redFaceFocused(){
    activeFace = "r";
    makeInstructionsPanel("rouge");
    makeButtonNext();
    makeFaceListPanel("", "", "active", "", "", "");
}
function yellowFaceFocused(){
    activeFace = "y";
    makeInstructionsPanel("jaune");
    makeButtonNext();
    makeFaceListPanel("", "", "", "active", "", "");
}
function orangeFaceFocused(){
    activeFace = "o";
    makeInstructionsPanel("orange");
    makeButtonNext();
    makeFaceListPanel("", "", "", "", "active", "");
}
function blueFaceFocused(){
    activeFace = "b";
    makeInstructionsPanel("bleue");
    makeButtonNext();
    makeFaceListPanel("", "", "", "", "", "active");
}
