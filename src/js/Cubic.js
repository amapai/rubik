/**
 * Represents a single cubic in the cube.
 * Used for the 3D representation of our cube.
 */
function Cubic(mat, x, y, z) {

     this.matrix = mat;
     /**
      * We translate to an extra half cubic to align cubics (center of rotation
      * align with rotation point).
      * Psst, it was a bug, don't touch it.
      */
     this.x = x+(cube.cubicSize/2); // position on the x'axe
     this.y = y+(cube.cubicSize/2); // position on the y'axe
     this.z = z+(cube.cubicSize/2); // position on the z'axe
     this.faces = [ new CubicFace([0, -1, 0], color(242, 201, 76)), // this.faces[0] : Yellow
                    new CubicFace([0, 1, 0], color(242, 242, 242)), // this.faces[1] : White
                    new CubicFace([0, 0, -1], color(45, 156, 219)), // this.faces[2] : Blue
                    new CubicFace([0, 0, 1], color(33, 150, 83)), // this.faces[3] : Green
                    new CubicFace([1, 0, 0], color(242, 153, 74)), // this.faces[4] : Orange
                    new CubicFace([-1, 0, 0], color(235, 87, 87))]; // this.faces[5] : Red

    /**
     * Display a single cubic in the canva.
     */
    this.display = function(){
        noFill();
        stroke(0);
        strokeWeight(1);
        push();
        translate(this.x, this.y, this.z);
        box(cube.cubicSize);
        for (let i = 0 ; i < this.faces.length ; i++) {
            this.faces[i].display();
        }
        pop();
    }


    /**
     * TODO
     */
    this.turnFaces = function(axe, angleDirection){
        for(let i = 0 ; i < this.faces.length ; i++){
            this.faces[i].turnFace(axe, angleDirection);
        }
    }
}
