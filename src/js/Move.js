/**
 * Represents a Cube movement.
 */
function Move(name, x, y, z, dir, nbTime, cubicSize){
    this.name = name;
    this.x = x*cubicSize-MAX_COORDONNEE_CUBE; // Rotation on the X axe
    this.y = y*cubicSize-MAX_COORDONNEE_CUBE; // Rotation on the Y axe
    this.z = z*cubicSize-MAX_COORDONNEE_CUBE; // Rotation on the Z axe

    this.dir = dir; // Clockwise or couter-clockwise
    this.angle = 0; // angle of the cubic's rotation
    this.animating = false; // flag
    this.finished = false; // flag
    this.nbTime = nbTime;
    this.cubicSize = cubicSize;
    this.isNxNMove = false;

    /**
     * Update function called when we create a NxN movement.
     * We multiply by nbRowsInvolved -1 because a cube is already counted.
     * Depending on the side of the movement on the plan, we have to add or sub.
     */
    this.updateMovementWithNbRows = function(nbRowsInvolved){
        this.isNxNMove = true;
        if (this.x <= MAX_COORDONNEE_CUBE) {
            if (this.x>0 ) {
                this.x -= cubicSize*(nbRowsInvolved-1);
            } else {
                this.x += cubicSize*(nbRowsInvolved-1);
            }
        }
        if (this.y <= MAX_COORDONNEE_CUBE) {
            if(this.y>0) {
                this.y -= cubicSize*(nbRowsInvolved-1);
            } else {
                this.y += cubicSize*(nbRowsInvolved-1);
            }
        }
        if (this.z <= MAX_COORDONNEE_CUBE) {
            if(this.z>0) {
                this.z -= cubicSize*(nbRowsInvolved-1);
            } else {
                this.z += cubicSize*(nbRowsInvolved-1);
            }
        }
    }

    /**
     * Initialise the flags and the angle.
     */
    this.start = function() {
        this.animating = true;
        this.finished = false;
        this.angle = 0;
    }

    /**
     * Allow to check if animation is done or in progress.
     */
    this.isFinished = function() {
        return this.finished;
    }

    /**
     * "Play" the movement.
     * The variable maxAngle change :
     * if the movement is of the move2 format, the angle is twice the usual : PI
     */
    this.update = function() {
        if (this.animating) {
            this.angle += this.dir * 0.05;
            let maxAngle = (this.nbTime!=1)?PI:HALF_PI;
            if (abs(this.angle) > maxAngle) {
                this.animating = false;
                this.finished = true;
            }
        }
    }
}
