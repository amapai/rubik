# Réunion du 11/03

## Participants

Thomas Georges
Amandine Paillard

## Ordre du jour

Montrer l'avancement du projet.

M. Montassier est content de l'avancement général.

## Interface

Il faudrait ajouter nos noms sur la page d'accueil. Il faut également penser à tout sourcer (référence aux algos et aux gits) vu que ce sera en ligne.

## Acquisition

Il aimerait une acquisition en temps réel (vu avec Florent) : faire tourner le cube, etc. Mais on va plutôt faire une classification.
Faire "mes cubes" pour calibrer notre cube une première fois. Laisser la possibilité de re-détecter les couleurs.

Si on finit plus tôt, on peut demander à dix personnes d'utiliser l'appli et voir combien de temps elles passent sur chacune des phases. Il aimerait toujours un suivi en cas d'erreur de mouvements (mais ça pourrait être un autre projet).

## Algo

Les algos sont bien. Pour le NxN, on devrait expliquer autant sur le côté code (expliquer les .txt) que sur les algos.
