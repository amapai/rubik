# Réunion du 7 septembre 2018, 9 h, faculté des sciences

## Ordre du jour

Présentation du projet à l'encadrant.
Retour sur le cahier des charges.

## Remarques

Nous avons pu nous rendre compte par nous même que ce projet est un thème récurrent, que ce soit l'idée de modéliser un Rubik's Cube ou d'implémenter des algorithmes de résolution. Cela ne posera pas de problème dans la notation du projet mais peut être moins motivant ou intéressant. On a évoqué le *Speed Resolution*, on peut réussir à finir un RC en 30 mouvements. De plus, ce projet implique en grande partie la notion de théorie des groupes : on peut passer d'un "groupe" à un différent en 20 mouvements (nombre de Dieu, algorithme de Dieu). Le problème est qu'on va vite se retrouver limité par les capacités de nos PCs car le graphe est trop grand...

Un des grands blocages auquel nous serons confrontés est la difficulté (non intuitif) à transformer notre RC physique en RC virtuel, si on fait cela par des clics associés à une palette de couleur, cela va vite être ennuyeux pour les utilisateurs (encore plus chiant si l'utilisateur se plante dès le premier mouvement et doit changer le modèle).

Réfléchir à ce qu'on appelle "vérifier la validité d'un RC" -> nécessite une plus ample réflexion. Enfin, nous devons prendre le temps de se mettre d'accord sur la base sur laquelle nous allons partir (si on adopte des manières de travailler différentes pour le projet, par exemple en Xtreme Programming qui implique un livrable "fini" et propre, on ne pourra pas utiliser le travail de l'autre groupe).

Les perspectives intéressantes et innovantes seraient de faire un passage d'un rubik physique à un rubik numérique et de le résoudre. Le résoudre avec des moyens fluides tel que la voix, les gestes ... et non d’une façon figée tel qu’utiliser les touches du clavier.

Il sera nécessaire de réfléchir à la structure de la représentation du rubik. Une modélisation linéaire avec une liste des cases est simple mais banale et basique OU un modèle basé sur la théorie des groupes. (A décider dans l'état de l'art)

Le partage des tâches est actuellement très disproportionné. Les algos serons assez fastidieux et compliqués à mettre en place alors que la GUI elle sera faite plus rapidement.

## À faire, avant le 24 septembre

* Définir un nouvel objectif, ajouter de la plus-value à notre projet.
* Réfléchir à une façon plus agréable de transformer le RC "physique" en RC "virtuel".
* Envoyer un mail à notre encadrant pour confirmer que nous souhaitons toujours travailler sur ce projet avec éventuellement notre nouvel objectif.
* Tester des solutions existantes pour avoir des idées.
* Regarder FrancoCube.
* Check différents articles scientifiques.

## Ressources

https://www.francocube.com/cyril/rubik_index
