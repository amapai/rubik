# Réunion du 03 octobre 2018

## 1. Participants

- Julie CAILLER
- Florent TORNIL
- M. MONTASSIER

## 2. Ordre du jour

### 2.1 Modélisation

- On a regardé ce qui existe rapidement en tenant compte de ses remarques
  - https://ruwix.com/online-rubiks-cube-solver-program/
  - http://rubiksolve.com
  - applis & comparatif  applis
  - démo cubeX
- Nouvelle vision du projet
  - mettre en avant la numérisation du cube
  - modélisation : rester sur du 2D ?
  - partir sur une application ?
    - facilité d'utilisation de la caméra
    - commandes vocales ?
    - décider lors de l'état de l'art

### 2.2 Algorithmique

- Calcul / algos
  - pattern matching
  - théorie des groupes
  - autres algos (manuels)
  - résolution à l'aveugle
- Le moins de coups
  - nombre de dieu

Partie Papiers
- Signature conventions

## 3. Idées à approfondir

### 3.1 Aqcuisition & modélisation

- Commandes vocales
- Réalité augmentée (voir casque carton samsung (geek fairies) & financement dept. info)
- Caméra frontale pour suivre les déplacements en temps réel & saisie plus pratique
- Régulateur de vitesse pour le défilement de l'algorithme

### 3.2 Algorithmique

- Outils de test massif pour les algorithmes
- Sauvegarder un **grand** ensemble de cubes différents pour appliquer les algos dessus (tests & stats)
- Recherche la pire position possible (*double couronne*) -> forcément 20 coups
- Validité du Rubik's cube

## 4. Dialogue

### 4.1 Aqcuisition & modélisation

- Défaut d'une application: trouver un bon framework. On risque de tomber sur de grosses usines sans avoir le temps d'apprendre ou de comprendre le fonctionnement. On nous conseille de contacter M. Seriai pour la partie développement mobile.
- Défaut d'un site / logiciel : communication avec la webcam -> regarder au niveau de Python
- La vision 3D du cube est quand même importante pour suivre la résolution en temps réel, plus proche de la vision du cube réel.

### 4.2 Algorithmique

- Il risque d'être compliqué de comprendre les articles très mathématiques sur le sujet
- L'algorithme résolvant le cube en 20 coups ou moins (nombre de dieu) est purement théorique et NP-Complet. L'implémenter rendrait **très très** riche
- L'intéret est dans la diminution du nombre de coups nécesasires à la résolution et l'obtention d'un certificat sur le nombre maximal de coups
  - Résoudre le cube : facile
  - Résoudre en moins de ~30 coups : beaucoup moins facile
- Se renseigner sur le algos existant autant au niveau théorique et algorithmique qu'au niveau implémentations :
  - arxiv
  - https://arxiv.org/search/?query=rubik%27s+cube&searchtype=all&source=header
  - http://web.mit.edu/sp.268/www/rubik.pdf
  - https://math.berkeley.edu/~hutching/rubik.pdf
  - http://math.utoledo.edu/~niverso/rubiks.pdf
- Au niveau de l'état de l'art, il faudra faire apparaitre les différents algorithmes ainsi qu'un *lien* vers leur implémentation si elle existe.

## 5. Divers

- Il faudra faire une vidéo de démonstration en tant que livrable avec une bonne voix grave et une grosse musique de fond ! :p
- Collègue de corée du sud
  - ILKYOO CHOI
  - Membre du WCA
  - 6e meilleur de son pays
  - ~3s
  - Vidéo sur son site à visionner

## 6. Contrat pro

- Du 1er juillet 2019 au 31 aout 2020
  - Possibilité de ne commencer qu'à partir de septembre (au choix de l'entreprise)
  - Entre 10 et 14 mois
- Il faut être bon dans le parcours et avoir l'accord du responsable de parcours
- Trouver une entreprise qui accepte
  - Certaines ont des avantages financiers, mais pas toutes
  - Offres transmises par M. Montassier
    - Souvent à partir de février
    - Validées par la fac
  - Valider l'entreprise et le sujet
- Souvent à but de pré-embauche
