\chapter{Interactions avec l'utilisateur}

Cette partie du projet concerne principalement le passage du cube physique à un modèle de ce cube sur l'application : son \og acquisition~\fg. Une fois cette étape importante accomplie, l'utilisateur peut interagir avec l'application. Il convient de rappeler les objectifs de l'application afin de pouvoir comparer différentes technologies sur ces mêmes aspects.

\section{Analyse de l'existant}

Lorsqu'une personne se retrouve face à un \rubiks, sa première idée est de tourner les faces de façon plus ou moins aléatoire en espérant arriver à la solution. Au bout d'un certain temps, en se rendant compte que cela ne fonctionne pas, cette personne va très probablement abandonner ou bien se diriger vers son ordinateur et utiliser son moteur de recherche favori afin de rechercher les termes \og Comment résoudre son \rubiks~\fg. Les résultats obtenus sont alors nombreux, allant des guides écrits aux vidéos. Si la personne est déterminée, elle ira suivre un de ces tutoriels afin de résoudre son cube. Cependant, avec une recherche plus poussée, il est possible de trouver de nombreux solveurs de \rubiks~en ligne. Une analyse plus détaillée de ces tutoriels et solveurs est disponible en annexe \ref{comparatif-sites}.

Bien que les tutoriels permettent d'apprendre à résoudre un \rubiks, ils ne proposent qu'une solution générale et ne tiennent pas compte de la configuration initiale du cube de l'utilisateur. Ils ne seront donc pas plus détaillés dans ce rapport.

Les solveurs en ligne existants présentent un problème majeur : l'acquisition du cube. En effet, colorer chaque cube un par un demande un minimum de 60 clics (54 cubes et 6 changements de couleur), en suivant précisément son cube physique sans se tromper. Cette solution est loin d'être efficace et de ce fait cette partie du projet mettra l'accent sur cette acquisition et sa simplification. L'aspect 3D du cube lors de son affichage à l'écran est également un avantage considérable dans la facilité de lecture des différents mouvements qu'il ne faudra pas négliger lors de ce projet.

Définir un cube virtuel et exécuter des algorithmes de résolution n'est pas réservé exclusivement aux ordinateurs. Un appareil mobile comme un téléphone portable peut également satisfaire ces objectifs. De plus, avec l'émergence des applications mobiles, les utilisateurs sont peut être plus à l'aise avec leur utilisation qu'ils ne le seraient avec un ordinateur. Cela fait d'autant de raison de s'intéresser à cette technologie.

Avant de s'intéresser aux moyens permettant de réaliser ce projet, il est opportun de se demander s'il n'existe pas déjà des applications similaires à ce qui est recherché. Une analyse plus détaillée de cette étude est disponible en annexe \ref{comparatif-app}. La plupart des applications étudiées permettent d'interagir avec le modèle virtuel en touchant l'écran ; il faut cependant être délicat car le modèle est très souvent sensible. Peu d'entre elles présentent un guidage "pas-à-pas" de la résolution du \rubiks~mais plutôt des modes d'emploi ou la théorie des méthodes existantes. Seule une application permet de photographier son \rubiks~; cette dernière est d'ailleurs la plus complète qui a pu être étudiée.

Enfin, une question demeure quant à la façon dont l'utilisateur va pouvoir photographier son \rubiks~et tenir son appareil mobile pendant qu'il le résout.

\section{Définition des besoins}

L'analyse de l'existant nous a permis de déterminer les fonctionnalités principales de l'application concernant la façon dont l'utilisateur interagit avec le modèle virtuel et l'application.

En parallèle de cet état de l'art, un état de livrable a été réalisé. Il reprend les besoins de l'application avec un aspect plus technique et visuel avec une maquette de la future application.

\subsection{Besoins initiaux}

Les besoins initiaux regroupent les fonctionnalités \og cœurs \fg~de l'application. Cette dernière doit être en mesure de fournir à l'utilisateur un moyen de passer de son \rubiks~réel à un modèle numérique, visualisable sur l'écran afin de suivre un algorithme de résolution.

L'acquisition d'un \rubiks~est le problème majeur des applications étudiées. En tenant compte des solutions proposées par celles-ci, il nous semble préférable d'utiliser des images du cube et de ses différentes faces et d'analyser ces images afin de déterminer la configuration du cube à acquérir, sur le principe de l'application CubeX décrite en \ref{annexe:cubex}.

La visualisation du cube correspond à la façon dont le cube va être présenté à l'utilisateur. En tenant compte des solutions proposées par les applications étudiées et de nos connaissances, une représentation du \rubiks~en 3D est préférable à celle en patron 2D pour simplifier la lecture des mouvements. Un représentation en réalité augmentée est aussi un avantage pour les mêmes raisons.

La résolution du cube permet à l'utilisateur de passer de sa configuration initiale à la configuration finale du cube. Celle-ci se doit d'être la plus simple et intuitive possible en se basant sur les choix de visualisation. Les principaux critères à prendre en compte sont :
\begin{description}
    \item[Le lancement de la résolution]: Une fois le cube acquis et affiché, l'utilisateur souhaite lancer la résolution. Cette action fait appel à un algorithme de résolution implémenté et détaillé dans la partie \ref{patie:resolution}. Une fois une suite de coups calculée par l'algorithme, l'application affiche une animation correspondant aux différents mouvements à effectuer.
    \item[Manipulation de l'animation]: Lors du déroulement de l'animation, il est préférable que l'utilisateur puisse interagir avec celle-ci, afin de la ralentir, l'accélérer, la mettre en pause ou encore revenir en arrière s'il pense s'être trompé ou a eu un moment d'inattention.
\end{description}

\subsection{Suggestions de fonctionnalités plus poussées}

Les fonctionnalités suivantes ne sont pas capitales pour la réalisation du projet, cependant elles constituent des améliorations souhaitables. Elles doivent alors être prises en compte pour le choix des technologies. 

Les commandes vocales permettent à un utilisateur d'interagir avec un produit, (appareil, logiciel) en s'adressant à lui oralement. Souvent la commande est préfixée par un mot-clé afin que le produit identifie que l'utilisateur s'adresse bien à lui. Pour l'application, cette technique permettrait à nos utilisateurs d'entièrement se focaliser sur la résolution du cube. Il n'aurait pas à toucher sa machine entre deux mouvements.

Lorsque l'utilisateur est concentré sur la résolution de son \rubiks~il est possible qu'il se trompe de mouvement en gardant les yeux rivés sur son écran et qu'il ne se rende pas compte de son erreur. Si l'application se contente d'afficher les mouvements de résolutions, une fois la résolution finie l'utilisateur se retrouvera avec un cube non résolu entre les mains. Cela ne serait pas arrivé si l'application avait proposé un suivi en temps réel.

\section{Outils à disposition}

Cette étape du projet est consacrée à l'étude et à la sélection des outils et technologies qui vont être utilisés lors du développement de l'application. Cette partie présente les technologies qui ont été étudiées. Elle est non-exhaustive et ne présente en aucun cas toutes les technologies existantes. 

\subsection{OpenCV}

OpenCV est une bibliothèque \textit{open-source} écrite en C++ et toujours maintenue à ce jour par \parencite{opencv}. Sa dernière version est la 4.0 et date de novembre 2018. Cette bibliothèque propose plus de 2500 algorithmes optimisés pour les domaines d'apprentissage automatique (\textit{machine learning}) et de vision par ordinateur (imagerie). Ces algorithmes peuvent être utilisés pour reconnaître des objets dans une image, des actions humaines dans une vidéo ou encore extraire des modèles 3D d'objets physiques.

Dans le cadre de ce projet, OpenCV permet de traiter rapidement et efficacement les photographies du cube afin d'en détecter la configuration ainsi que d'interpréter les mouvements effectués par l'utilisateur pour s'assurer que ce dernier suive bien les instructions qui lui sont proposées.

OpenCV ne propose cependant aucune solution pour la prise d'images : ces dernières doivent être fournies au programme ce qui signifie que l'utilisateur doit au préalable prendre et fournir les photographies de son cube par lui-même. De la même façon, la reconnaissance des actions dans une vidéo nécessite qu'une vidéo soit fournie au programme et aucune solution n'est proposée au traitement \og en direct \fg~des actions.

\subsection{Processing et P5.js}

Processing est un logiciel et un langage éducatif dans le contexte des arts visuels \parencite{processing}. Publié en 2001, Processing est gratuit, \textit{open-source} et disponible sur GNU/Linux, Mac OS X, Windows et même Android ou ARM. Depuis sa création, de nombreuses librairies étendant le programme principal ont vu le jour permettant de réaliser facilement des programmes interactifs en utilisant des technologies 2D, 3D, PDF ou encore SVG. Les traitements 2D et 3D sont optimisés grâce à une intégration de la bibliothèque OpenGL dans le cœur de Processing. Ce logiciel ayant été publié il y a maintenant dix-sept ans, sa documentation est complète et de nombreux ouvrages existent à son sujet publiés aussi bien par des chercheurs que des artistes ou des étudiants.

P5.js \parencite{p5js} est une librairie Javascript reprenant les objectifs principaux de Processing tels que la facilité d'accès à la programmation aux artistes ou designers et de moderniser la programmation web.

Cette librairie permet d'accéder facilement à la webcam de l'utilisateur afin d'obtenir les images nécessaires. De plus, de nombreuses librairies supplémentaires permettent de traiter les images obtenues, mais aussi de capter, reconnaître ou émettre des sons.

\subsection{Application mobile}

L'idée de développer une application mobile a été motivée par la facilité avec laquelle une application peut utiliser des clichés stockées. Cet outil permet de rendre l'utilisation de l'application plus agréable à l'utilisateur de part sa plus grande maniabilité et l'aisance des utilisateurs à interagir avec un téléphone portable. \\ Cependant, si l'utilisation d'un appareil mobile peut faciliter l'acquisition du modèle, l'application risque d'être limité par la puissance de calcul du téléphone. Les algorithmes de résolution du \rubiks~ peuvent être longs et certains appareils pourraient avoir des difficultés à les faire fonctionner. Si cela s'avère être un problème, une solution envisageable est de donner la responsabilité d'exécuter ces algorithmes à un service web. L'application mobile se chargerait uniquement de l'acquisition du modèle, d'envoyer ce modèle au service web et de récupérer la série de mouvements servant à résoudre le cube puis de la mettre en forme pour l'utilisateur.\\ Toutefois, il convient de rappeler que parmi les applications existantes testées, il n'y avait pas eu de problème quant au calcul de la solution (et ce même quand le téléphone était hors connexion).

De plus, de nombreux kits de développement et API existent permettant de faciliter une partie du traitement de l'application, que ce soit l'acquisition du modèle ou l'affichage des mouvements de résolution. Par exemple, Google a développé une API facilitant la reconnaissance d'image : Cloudvision \parencite{cloudvision}. Cette API permet d'analyser une image et de reconnaître des formes, des couleurs ou du texte composant cette image.\\Tous les kits de développement existants ne peuvent pas être listés ici, certains ont néanmoins été étudiés dans le cadre d'une fonctionnalité précise.

\subsection{Autres technologies intéressantes}

L'application doit être facile d'utilisation pour les utilisateurs. Dans ce but, certaines technologies semblent prometteuses. En plus de rendre l'utilisation de l'application intuitive, elles sont novatrices et apportent une réelle plus-value.

\subsubsection{Réalité augmentée}

La réalité augmentée permet d'insérer des éléments virtuels dans une image réelle. En utilisant cette technologie, les mouvements à faire pour résoudre le \rubiks~pourraient venir se superposer au modèle physique. Cela permettrait de rendre l'utilisation plus intuitive. De nombreuses façons d'implémenter cette technologie sont disponibles ici : \url{http://socialcompare.com/fr/comparison/augmented-reality-sdks}.

\begin{description}
    \item[ARCore] : kit de développement développé par Google \parencite{arcore}, peut être utilisé avec les environnements de développement Java, iOS, Unity ou encore Unreal.
    \item[ARToolKit] : librairie \textit{open-source} développée par	Hirokazu Kato depuis 1999 puis acquise bien plus tard par DAQRI \parencite{artoolkit}. Très utilisée dans le milieu de la réalité augmentée, la librairie permet de créer un modèle cohérent et robuste qui s'adapte à chacun des mouvements de la caméra. ARToolKit est disponible sur de nombreux environnements tels qu'Android, iOS, Windows, Linux ou Mac OS.
    \item[Vuforia] : plateforme de réalité augmentée leader du marché. L'entreprise  possédant Vuforia, \parencite{vuforia} met à disposition de nombreux outils (base de données, kits de développement, ....) et accompagne également les entreprises désirant créer leur propre application de réalité augmentée.
    \item[Wikitude SDK] \parencite{wikitude} : Très similaire à Vuforia, pour une utilisation plus professionnelle.
\end{description}

\subsubsection{Réalité virtuelle}

Contrairement à la réalité augmentée, la réalité virtuelle crée un environnement virtuel se rapprochant d'un environnement réel. Par exemple un simulateur de vol utilise la réalité virtuelle ; concrètement l'utilisateur reste assis sur son siège mais dans la simulation, il est en train de piloter un avion. Pour en revenir à l'application, seule la résolution du \rubiks~intéresse l'utilisateur. Le \rubiks~est le seul élément qui a besoin d'être rendu virtuel. C'est pourquoi cette technique ne semble pas pertinente ici. 

\section{Discussion}

Le tableau suivant reprend les principales fonctionnalités composant l'application et compare les outils étudiés sur ces dernières.

\begin{tabular}{|l|c|c|c|}
   \hline
   Fonctionnalités souhaitées & OpenCV & P5.js & Application mobile \\
   \hline
   Prise de clichés & & $\checkmark$ & $\checkmark$ \\
   \hline
   Traitement d'images & $\checkmark$ & $\checkmark$ & $\checkmark$ \\
   \hline
   Modèle 3D & $\checkmark$ & $\checkmark$ & $\checkmark$ \\
   \hline
   Visualisation de la résolution & $\checkmark$ & $\checkmark$ & $\checkmark$ \\
   \hline
   Reconnaissance vocale &  & $\checkmark$ & $\checkmark$ \\
   \hline
   Suivi en temps réel &  & $\checkmark$ & $\checkmark$ \\
   \hline
\end{tabular}

\textit{P5.js} et les applications mobiles permettent tous les deux de réaliser les fonctionnalités de l'application. Cependant si le suivi en temps réel est implémenté, pour une application mobile cela implique de garder le téléphone face au cube tout en le résolvant, ce qui semble peu pratique. C'est pourquoi la décision finale porte sur \textit{P5.js}.