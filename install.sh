#!/bin/bash
# Installation du serveur apache
sudo apt-get install apache2

# Activation de php sur le serveur
sudo apt-get install php libapache2-mod-php
sudo a2enmod mpm_prefork && sudo a2enmod php7.2

# Redémarrer le serveur après les changements
sudo service apache2 restart

# Déplacement des sources dans le dossier du serveur
sudo cp -r src /var/www/html

echo ""
echo ""
echo "Maintenant l'application va se lancer avec la commande :"
echo "> google-chrome localhost/src"
echo "Si Google chrome n'est pas votre navigateur par défaut, utilisez votre navigateur. Vous ne pourrez pas profiter de la reconnaissance vocale."
# Naviguer sur localhost/src (Chrome pour reconnaissance vocale)
google-chrome localhost/src
